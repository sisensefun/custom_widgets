

mod.directive('dashboardLayoutCell', [function () {

    return {
        restrict: 'C',
        link: function (scope, element, attrs) {
            
            scope.declareTabberWidgetsCell = function () {
                // apply width;
                $(element).css('width', '100%').css('min-width', '100%').css('max-width', '100%');
                $(element).addClass('hide-resizers');

                delete scope.declareTabberWidgetsCell;
            }
        }
    }
}]);

mod.directive('dashboardLayoutSubcellHost', [function () {
    
    return {
        restrict: 'C',
        link: function (scope, element, attrs) {
            
            // validate.
            if (!scope.column.$$tabbers) {
                return;
            }

            // get tabbers metadata definition.
            var meta = scope.column.$$tabbers[scope.column.cells.indexOf(scope.cell) - 1];

            // validate.
            if (!angular.isObject(meta) || !meta.width) {
                return;
            }

            // declare cell.
            if (scope.declareTabberWidgetsCell) {
                scope.declareTabberWidgetsCell();
            }


            //if type multiply skip width resizing
             if(!meta.multiply) {
                 // apply style.
                  $(element).css('width', meta.width).css('min-width', meta.width).css('max-width', meta.width);
             }
        }
    }
}]);

mod.directive("widget", [function () {
    
    return {
        restrict: 'E',
        link: function (scope, element, attrs) {
            
            scope.$watch('widget.type', function (type) {
                
                // validate.
                if (type != 'WidgetsTabber' || !scope.subcell || scope.subcell.index !== 0) {
                    return;
                }

                // count tabbers on the same row.
                countTabbers = scope.cellMeta.subcellsCount;

                // validate.
                if (!countTabbers) {
                    return;
                }

                // ensure tabber metadata is initiated.
                if (!scope.column.$$tabbers) {
                    scope.column.$$tabbers = {};
                }
                
                var cellIndex = scope.column.cells.indexOf(scope.cell);


                scope.column.$$tabbers[cellIndex] = {
                    cellIndex: cellIndex,
                    tabbersCount: countTabbers,
                    width: (100 / countTabbers) + '%'
                };

                if(scope.widget.tabsConfig){
                    scope.column.$$tabbers[cellIndex].multiply = true;
                }

            });


            // how to call - prism.$ngscope.$broadcast('tab-change', { hide: toHide(n), show: n });
            scope.$on('tab-change', function (ev, args) {

                // validate.
                if (!args) {
                    return;
                }

                var inConfig = false;

                // show
                if (angular.isArray(args.show) && args.show.indexOf(scope.widget.oid) > -1) {
                    $(element).removeClass('dontshowme').addClass('showjustme');
                    $(element).closest('.dashboard-layout-subcell-host').removeClass('dontshowme-parent').addClass('showjustme-parent');
                    inConfig = true;
                } else if (angular.isArray(args.hide) && args.hide.indexOf(scope.widget.oid) > -1) {
                    $(element).removeClass('showjustme').addClass('dontshowme');
                    $(element).closest('.dashboard-layout-subcell-host').removeClass('showjustme-parent').addClass('dontshowme-parent');
                    inConfig = true;
                }

                if(args.tabsConfig !== 'multiply' && inConfig ){

                    // fix scrolling
                    $(element).closest('.dashboard-layout-subcell-host').find('.ui-resizable-handle').css('display','none');
                }

            });
        }
    }
    
}]);
