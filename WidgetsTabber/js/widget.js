/**
 * @fileOverview    Widgets Tabber
 * @author          Dana Raijman
 * @version         V1.0
 * @SiSenseVersion  V5.7.6
 **/

prism.registerWidget("WidgetsTabber", {
	name : "WidgetsTabber",
	family : "WidgetsTabber",
	title : "Tabber",
	iconSmall : "/plugins/widgetstabber/styles/tabs.png",
	styleEditorTemplate : '/plugins/WidgetsTabber/js/styler.html',
	style : {activeTab: 0},
	// sizing must be stated
	sizing: {
		minHeight: 32, //header
		maxHeight: 2048,
		minWidth: 128,
		maxWidth: 2048,
		height:32,
		defaultWidth: 512
	},
	data : {
		selection : [],
		defaultQueryResult : {},
		panels : []
	},
    hideNoResults : true,
	render : function (widget, e) {

        // skip if no tabs defined
        if (!widget.tabs)
            return;

	    var
            lastSelected,
            tabs = widget.tabs,
            $lmnt = $(e.element),
            selectedColor  = e.selectedColor || 'rgb(39, 204, 93)',
            fontColor  = e.fontColor || '#cccccc',
            prefixText = e.prefixText || '',
            suffixText = e.suffixText || '',
            descColor = e.descColor || "#a5a5a5",
            height = e.height || 32,
            parentWidth = $$get(widget, 'style.width') || e.elementWidth || '100%',
            desc = prefixText + ' '+ (widget.desc ? widget.desc : ''),
            $desc = $('<span class="descDefaultCSS" >' + desc + '&nbsp &nbsp'+ '</span>').css('color', descColor),
            isBlank = function(str) {
                return (!str || /^\s*$/.test(str));
            },
            tabsList = $('<div class="listDefaultCSS"></div>'),
            defaultIndex = widget.tabs.indexOf(_.find(widget.tabs, function (item) {
                return item.default;
            }))
            ;

        defaultIndex = defaultIndex != -1 ? defaultIndex : widget.style.activeTab;

        //adds widget description before tab list
        if(desc && !isBlank(desc)){
            tabsList.append($desc);
        }

        for (var i = 0; i < tabs.length; i++) {

            var $listItem = $('<span class="listItemDefaultCSS">' + tabs[i].title + '</span>');

            $listItem.attr('index', i).on('click', tabClicked);
            tabsList.append($listItem);

            // add separators between tab list items
            if (i != tabs.length - 1)
                tabsList.append('<div class="vSeparatorCSS"></div>');

            // set active tab
            if (i == defaultIndex)
                select($listItem);
        }

        if (suffixText.length) {
            var $suffixText = $('<span class="descDefaultCSS">' + '&nbsp' + suffixText + '' + '</span>').css('color', descColor);
            tabsList.append($suffixText)
        }

        $lmnt.parent().addClass('parentCSS');
        $lmnt.empty().addClass('defaultCSS').append(tabsList);

        // sets user css options
        $lmnt.css({
            'height': height,
            'color': fontColor,
            'width': parentWidth
        });


        if (!prism.$ngscope.dashboard.editing) {
            $lmnt.parent().children('widget-header').remove();
            $lmnt.height(24);
        }

        function tabClicked(){
            select(this);
            hideDisplayWidgets(this);
        }

        function hideDisplayWidgets(element){

            var index = $(element).attr('index');

            //skip if no index
            if (!index) return;

            var item = tabs[index];

            prism.$ngscope.$broadcast('tab-change', {
                show: item.displayWidgetIds || [],
                hide: item.hideWidgetIds || [],
                tabsConfig: widget.tabsConfig || ''
            });

            var index = $(element).attr('index');
        }

        function select(item){
            var
                $item = $(item),
                indexCurrentItem = $item.attr('index')
                ;

            if (lastSelected) {
                $(lastSelected).css('color', fontColor);
            }

            $item.css('color', selectedColor);
            lastSelected = $item;

            widget.style.activeTab = indexCurrentItem;

            //  saving changes on server
            widget.changesMade('activeTab', 'style');

            //calls dashboard scrollbar update event (directive "layoutScroll")
            prism.$ngscope.$broadcast('scrolledContentLoaded');
        }

            hideDisplayWidgets(lastSelected);
	},
	destroy : function (e) {}
});
