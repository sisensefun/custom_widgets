+************************************************+
* SiSense Widgets Tabber *
+************************************************+

-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
Description:
-- Add the ability to avoid overcrowded dashboards, with many different charts and widgets,
to place several very similar widgets, one next to another, and display only selected ones.
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------

How to use:
-----------

1) Create a Tabber custom widget
3) Edit a tabber widget script, configure the different tabs:
 *
 * example:
 * 	widget.tabs = [{title: "Tab1", displayWidgetIds : ["556c26f88a6b1c68290000f8"], hideWidgetIds : ["556eb860106eb3f42100002f"]},
   			   {title: "Tab2", displayWidgetIds : ["556eb860106eb3f42100002f"], hideWidgetIds : ["556c26f88a6b1c68290000f8"]}];
3) In case you like to change default configuration as described bellow edit the widget script: widget.on('render',function(w,e){});
setting e to the configuration you'd like to change.
 *
 * example:
 *  widget.on('render',function(w, e){e.selectedColor = 'red';});
4) Edit dashboard script, define hidden widgets:
 *
 * example
 * dashboard.hideWidgetIds = ["556eb860106eb3f42100002f","556c26f88a6b1c68290000f8"];

5) To Add more than one widget to tab, please add widget.tabsConfig = 'multiply';


Functionality:
-------------
Thinning out the dashboards load, and making your previously overcrowded dashboard easier on the eye.

Tab Parameter:

Key			        Description													Type				
-------------------------------------------------------------------------------------------------
title		        Tab title													String
displayWidgetIds    List of widget ids to display when the tab is selected      Array of strings
hideWidgetIds	    List of widget ids to hide when the tab is selected         Array of strings
default				Is it default tab											Boolean


Tabs Custom Configuration:

Key			        Description													Type				DefaultValue
------------------------------------------------------------------------------------------------------------------
selectedColor		Selected tab color											String				'#86b817'
fontColor			Font color													String				'#cccccc'																
prefixText			Prefix text for the tab list								String
suffixText			Suffix text for the tab list								String																
elementWidth		Tab list width												String				'103%'
descColor			Tab description color										String				'#a5a5a5'
parentMarginLeft	Margin left if tab list from parent							String				'-15px'
height				Tab list height												Number				32

