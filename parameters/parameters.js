//
// parameters plugin.
//

prism.run(["$jaql", function ($jaql)
{
	// 
	// Query Handling
	// --------------

	// drill through the given object and look for available parameters in the context matching any of the object's placeholders
	function drill(o, context) {

		var v, t, f, m, a;
		
		for (var p in o) {

			if (!o.hasOwnProperty(p)) {

				continue;
			}

			v = o[p];

			// looking for placeholder
			if (defined(t = $$get(v, "title")) && typeof t === 'string' && t[0] === '@') {

				t = t.substring(1);
				a = m = context[t];
				
				if (!defined(m) && t.endsWith(".value")) {
					
					t = t.substring(0, t.indexOf(".value"));
					a = context[t];
					m = parseFloat($$get(a, "filter.equals"));
				}

				// custom value provider
				if (defined(context.$custom)) {

					m = null;
					a = context.$custom(t) || a;
				}

				
				if (defined(m)) {
					
					// applying value
					o[p] = m;
					
					if (defined(a, "$$guid")) {
						removeItemFromArray(context.$metadata, context.$filtersmap[a.$$guid]);
					}
				}
			}

			// continue drilling
			if (Array.isArray(v) || typeof (v) === "object") {
				
				drill(v, context);
			}
		}
	}

	function removeItemFromArray (array, item) {
		!!window.sisenseUtils ? window.sisenseUtils.array.remove(array, item) : array.remove(item);
	}

	//
	// generate name -> value context map that will be used to apply parameter values to query placeholders. 
	// Also, a custom value provider can be used when attached to the widget.options object to allow resolving placeholders values in ways exstinging the default widget/dashboard filters.
	//
	// I/E -
	//
	// dashboard.$customParameterValueProvider = function(name) {
	//		
	//		if (name === "A") return { ... };
	// }
	function buildContext(context, arr) {

		var f, t;

		for (var i = 0 ; i < arr.length ; i++) {

			f = arr[i];
			t = $$get(f, "jaql.title");

			if (defined(t)) {

				context[t] = f.jaql;

				//
				// mapping source -> query item to remove filters that were used as parameter value
				var mitem = context.$metadata.filter(function (m) { return m.panel === "scope" && !$jaql.isMeasure(m) && $$get(m, "jaql.dim") === $$get(f, "jaql.dim") && $$get(m, "jaql.level") === $$get(f, "jaql.level") });
				if (mitem.length === 1) {
					 
					f.jaql.$$guid = $$guid(8);		
					context.$filtersmap[f.jaql.$$guid] = mitem[0];
				}
					
			}
		}
	}
	function getContext(widget, query) {

		var context = {};
		context.$filtersmap = {};
		context.$metadata = query.metadata;

		buildContext(context, widget.dashboard.filters.$$items);
		buildContext(context, widget.metadata.filters());

		// setting custom provider
		context.$custom = $$get(widget.dashboard, "$customParameterValueProvider");

		return context;
	}

	// gets the widget before query event and tryng to apply parameter values to existing query placeholders
	function processWidgetQuery(s, e) {

		var widget = e.widget;
		var query = e.query;

		// generating context
		var context = getContext(widget, query);
		
		// applying parameter values
		drill(query.metadata, context);

		// cleaning $$guid attributes from context items
		for (var p in context) {

			if (!context.hasOwnProperty(p)) {

				continue;
			}

			if (defined(context, p+".$$guid")) {

				delete context[p].$$guid;
			}
		}
	}




	// 
	// Hooking-up
	// ----------

	var handler;

	// uses to intercept all widget queries of a given dashboard and apply dynamic values into predefined placeholders 
	function dashboardHandler(dashboard) {

		// release all references held by the dashboard handler
		this.destory = function () {

			dashboard.off("widgetbeforequery", processWidgetQuery);
		};

		// hooking up to the dashboard
		dashboard.on("widgetbeforequery", processWidgetQuery);
	}

	// hooking up to all dashboard queries on load
	prism.on("dashboardloaded", function (e, args) {

		handler = new dashboardHandler(args.dashboard);
	});

	// removing hooks from dashboard on unload
	prism.on("dashboardunloaded", function () {
		
		if (!defined(handler)) {

			return;
		}

		handler.destory();
		handler = null;
	});
}]);


