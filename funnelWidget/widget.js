
prism.registerWidget("funnel", {

	name : "funnel",
	family : "Column",
	title : "Funnel",
	iconSmall : "/plugins/funnelWidget/widget-24.png",
	styleEditorTemplate: "/plugins/funnelWidget/styler.html",
	style: {
		isCurved: false,
		bottomPinch: 0,
		fillType: "solid",		//can also be 'gradient'
		isInverted: false,
		hoverEffects: true,
		dynamicArea: true
	},
	data : {
		selection : [],
		defaultQueryResult : {},	
		panels : [
			{
				name: 'Category',
				type: "series",
				itemAttributes: ["color"],
				allowedColoringTypes: function() {
					return {
						color: true,
						condition: false,
						range: false
					}
				},
				metadata: {
					types: ['dimensions'],
					maxitems: 1
				},
				visibility: function(w) {
					return w.metadata.panel("value").items.length < 2
				}
			},
			{
				name: 'value',
				type: 'visible',	
				itemAttributes: ["color"],
				allowedColoringTypes: function() {
					return  {
						color: true,
						condition: false,
						range: false
					};
				},
				metadata: {
					types: ['measures'],
					maxitems: function(w) {
						if (w.metadata.panel("Category").items.length == 0) {
							return -1;
						} else {
							return 1;
						}
					}
				},
				itemAdded: function(widget, item) {
					var colorFormatType = $$get(item, "format.color.type");
					if ("color" === colorFormatType || "color" === colorFormatType) {
						var color = item.format.color.color;
						defined(color) && "transparent" != color && "white" != color && "#fff" != color && "#ffffff" != color || $jaql.resetColor(item)
					}
					"range" === colorFormatType && $jaql.resetColor(item), defined(item, "format.color_bkp") && $jaql.resetColor(item), defined(item, "format.members") && delete item.format.members
				}
			},			
			{
				name: 'filters',
				type: 'filters',
				metadata: {
					types: ['dimensions'],
					maxitems: -1
				}
			}
		],
		
		canColor: function (widget, panel, item) {
			return (panel.name === "value" && widget.metadata.panel("Category").items.length == 0);
            //return panel.name === "value" ;
        },
		
		allocatePanel: function (widget, metadataItem) {			
			// measure
			if (prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel("value").items.length === 0) {

				return "value";
			}
			// dimension
			else if (!prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel("Category").items.length < 3) {

				return "Category";
			}
		},

		// returns true/ reason why the given item configuration is not/supported by the widget
		isSupported: function (items) {

			return this.rankMetadata(items, null, null) > -1;
		},

		// ranks the compatibility of the given metadata items with the widget
		rankMetadata: function (items, type, subtype) {

			var a = prism.$jaql.analyze(items);

			// require 1 measure, 2 dimension
			if (a.measures.length == 2 && a.dimensions.length == 1) {

				return 0;
			}

			return -1;
		},

		// populates the metadata items to the widget
		populateMetadata: function (widget, items) {

			var a = prism.$jaql.analyze(items);

			// allocating dimensions
			widget.metadata.panel("Category").push(a.dimensions);
			widget.metadata.panel("value").push(a.measures);

			// allocating filters
			widget.metadata.panel("filters").push(a.filters);
		},
		
		// builds a jaql query from the given widget
		buildQuery: function (widget) {
	
			// building jaql query object from widget metadata 
			var query = { 
				datasource: widget.datasource, 
				format: "json",
				isMaskedResult:true,
				metadata: [] 
			};

			// using categories or multiple data series?
			if (widget.metadata.panel("Category").items.length == 0) {
				// pushing all values
				for (i=0; i<widget.metadata.panel("value").items.length; i++) {
					query.metadata.push(widget.metadata.panel("value").items[i]);
				}
				
			} else {
				// Get the dimension	
				for (i=0; i<widget.metadata.panel("Category").items.length; i++) {
					query.metadata.push(widget.metadata.panel("Category").items[i]);
				}
								
				// pushing values
				for (i=0; i<widget.metadata.panel("value").items.length; i++) {
					query.metadata.push(widget.metadata.panel("value").items[i]);
				}				
			}
			// force a sort by 
			if ($$get(query, 'metadata.1.jaql')) {
				query.metadata[1].jaql.sort = "desc";
			}

			// pushing filters
			widget.metadata.panel('filters').items.forEach(function (item) {
				item = $$.object.clone(item, true);
				item.panel = "scope";
				query.metadata.push(item);
			});
			return query;
		},

		//create highcharts data structure
		processResult : function (widget, queryResult) {		
			//format data
			var newResults = [];
			var hasCategories = (widget.metadata.panels[0].items.length > 0);

			//	Function to change color of selected dim
			var lighter = function(color, luminosity) {

				// validate hex string
				color = new String(color).replace(/[^0-9a-f]/gi, '');
				if (color.length < 6) {
					color = color[0]+ color[0]+ color[1]+ color[1]+ color[2]+ color[2];
				}
				luminosity = luminosity || 0;

				// convert to decimal and change luminosity
				var newColor = "#", c, i, black = 0, white = 255;
				for (i = 0; i < 3; i++) {
					c = parseInt(color.substr(i*2,2), 16);
					c = Math.round(Math.min(Math.max(black, c + (luminosity * white)), white)).toString(16);
					newColor += ("00"+c).substr(c.length);
				}
				return newColor; 
			}

			if (hasCategories) {
				//using categories
				var categoryColors = widget.metadata.panels[0].items[0].format.members;
				
				//	Create the data objects
				for (i=0; i<queryResult.$$rows.length; i++) {
					
					//	Get the data values needed
					var thisText = queryResult.$$rows[i][0].text,
						thisData = ($$get(queryResult, '$$rows.' + i + '.1.data') || 1).toString(),
						thisDataFormatted = ($$get(queryResult, '$$rows.' + i + '.1.text') || "1"),
						thisColor = categoryColors[queryResult.$$rows[i][0].text].color;

					//	Change the color for selected members (on highlight mode)
					if (widget.options.dashboardFiltersMode == "select" && !queryResult.$$rows[i][0].selected) {
						thisColor = lighter(thisColor,0.5);
					}
					
					//	Create the data array
					newResults[i] = [ thisText, thisData, thisColor, thisDataFormatted ];
				}
			} else {
				// using multiple data series, instead of categories				
				for (i=0; i<queryResult.$$rows[0].length; i++) {
					var dataPoint = queryResult.$$rows[0][i];
					var dataLabel = widget.metadata.panels[1].items[i].jaql.title;
					var dataColor = widget.metadata.panels[1].items[i].format.color.color;
					newResults[i] = [ dataLabel, dataPoint.data.toString(), dataColor, dataPoint.text]; 					
				}
			}
			
			
			return newResults;
		}
	},
	//	Check to make sure the dashboard filter doesn't filter out members when highlight
	beforequery: function(widget,event) {

			//	Is the widget set to highlight mode?
			if (widget.options.dashboardFiltersMode === "select") {

				//	Check for a dimension
				var firstItem = event.query.metadata[0];				
				if (firstItem.jaql.datatype !== "numeric") {

					//	Check to see if a dashboard filter matches this dimension
					for (var i=0; i<event.query.metadata.length; i++){
					
						//	Get this item
						var item = event.query.metadata[i];

						//	Found a match						
						var needFix = (item.panel == "scope") && (item.jaql.dim == firstItem.jaql.dim) 
						if (needFix){

							//	Add the filter to the dimension's metadata
							firstItem.jaql['in'] = {
								"selected": {
									"jaql": item.jaql
								}
							};

							//	Set the inResultset attribute for the format
							$.each(firstItem.format.members, function(){
								this['inResultset'] = true;
							})

							//	Remove this item from the metadata array
							event.query.metadata.splice(i,1);
						}
						
					}
				}
			}

		},
	render : function (s, e) {
	
		// Get widget elements
		var $lmnt = $(e.element);
		$lmnt.empty();
		
		var width = $lmnt.width(),
			height = $lmnt.height();
		
		// Prepare the container div
		var MyDiv = $lmnt[0],
			ObjectID = s.oid,
			ChartDivName = "Funnel-" + ObjectID;
			
		if (!defined(MyDiv)) {
			return;
		}
			
		MyDiv.setAttribute("id",ChartDivName);
		MyDiv.setAttribute("style","width: 99%; height: 99%; margin: 0 auto");

		//	Define function for click hander
		var clicked = null;
		if (s.options.selector) {
			clicked = function(d) {

				//	Get the widget id
				var oid = $(this).closest('widget').attr('widgetid');

				//	Get the widget object
				var widget = $.grep(prism.activeDashboard.widgets.$$widgets,function(w){
					return w.oid == oid;
				})[0]

				//	Make sure a matching widget was found
				if (widget) {

					//	Get the metadata
					var metadata = widget.rawQueryResult.metadata[0];

					//	Get the value
					var value = widget.rawQueryResult.values[d.index];

					//	Create the jaql object
					var filterJaql = {
						jaql: {
							dim:metadata.jaql.dim,
							title:metadata.jaql.title,
							table:metadata.jaql.table,
							column:metadata.jaql.column,
							datatype: metadata.jaql.datatype,
							filter: {
								members: [value[0].data]
							}
						}
					};

					//	Create the options object
					var filterOptions = {
						save:true, 
						refresh:true
					};
					
					//	Set the filter
					prism.activeDashboard.filters.update(filterJaql, filterOptions);

				}
				
			}
		}
		
		// Create options object
		var options = {
			isCurved: s.style.isCurved,
			bottomPinch: s.style.bottomPinch,
			fillType: "gradient",
			isInverted: s.style.isInverted,
			hoverEffects: true,
			dynamicArea: s.style.dynamicArea,
			onclick: clicked
		};
		
		//	Make sure there's something to draw
		if (!$.isEmptyObject(s.queryResult)) {
			// Build chart
			var chart = new D3Funnel("#" + ChartDivName);
			
			if (options.isInverted) {
				//invert the data
				chart.draw(s.queryResult.reverse(), options);
			} else {
				// Otherwise, just use the regular data
				chart.draw(s.queryResult, options);
			}
		}
		
		
	},


	destroy : function (s, e) {}
});