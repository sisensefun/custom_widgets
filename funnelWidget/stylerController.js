
mod.controller('stylerController', ['$scope',
    function ($scope) {

        /**
         * variables
         */


        /**
         * watches
         */
        $scope.$watch('widget', function (val) {

            $scope.model = $$get($scope, 'widget.style');
        });



        /**
         * public methods
         */

        $scope.inverse = function (isInverted) {
        	$scope.model.isInverted = isInverted;
        	_.defer(function () {
        		$scope.$root.widget.redraw();
        	});
        };
		
		$scope.dynamicArea = function (dynamicArea) {
        	$scope.model.dynamicArea = dynamicArea;
        	_.defer(function () {
        		$scope.$root.widget.redraw();
        	});
        };
		
		$scope.bottomPinch = function (bottomPinch) {
        	$scope.model.bottomPinch = bottomPinch;
        	_.defer(function () {
        		$scope.$root.widget.redraw();
        	});
        };
		
		$scope.isCurved = function (isCurved) {
        	$scope.model.isCurved = isCurved;
        	_.defer(function () {
        		$scope.$root.widget.redraw();
        	});
        };
    }
]);