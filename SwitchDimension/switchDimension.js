prism.registerSwitchDimension = function (options) {

	var widget = options.widget;
	var dimensionSettings = options.dimensions;
	widget.on("initialized", function () {
	
		var unregister = prism.on("beforemenu", function (e, args) {

			if (args.settings.name != "datapoint" || args.settings.widget.oid != widget.oid || !dimensionSettings) {
				return;
			}

			dimensionSettings.forEach(function(settings){
				var dim = settings.dim;
				var panel = settings.panel;
				var dimPanelItemIndex = settings.index;
				var title = settings.title;
				var agg = settings.agg;
				var level = settings.level;
				var widgetTitle = settings.widgetTitle;
				var datatype = settings.datatype;

				if(!dim || !panel || dimPanelItemIndex === undefined) {
					console.log('Invalid parameters');
					return;
				}
				var dimensionItemJaql = widget.metadata.panel(panel).items[dimPanelItemIndex].jaql;
				var isCurrentDimension = dimensionItemJaql.dim.toLowerCase() == dim.toLowerCase();
				var dimFormatted = dim.replace(/[\[\]']+/g,'');

				if(isCurrentDimension) return;

				args.settings.items.push({ type: "separator" });

				args.settings.items.push({

					caption:  "Switch To " + (title || dim),
					closing: true,
					execute: function () {	
				        try{
							dimensionItemJaql.dim = dim;
							dimensionItemJaql.title = title;
							dimensionItemJaql.datatype = datatype;
							dimensionItemJaql.agg = agg || dimensionItemJaql.agg;
							dimensionItemJaql.level = level || dimensionItemJaql.level;
							dimensionItemJaql.column = dimFormatted.substr(dim.indexOf('.'), dim.length);
							dimensionItemJaql.table = dimFormatted.substr(0, dim.indexOf('.') - 1);
							widget.title = widgetTitle || widget.title;
							widget.changesMade();
							widget.refresh();
						}
                        catch(e){
                            console.log(e);
                        }

					}
				});
			});
		});

		widget.on("destroyed", function () {
			unregister();
		});

	});
};