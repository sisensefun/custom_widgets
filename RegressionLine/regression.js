//	Initialize the copyWidget object
prism.run(["$filter", function($filter) {	

	//////////////////////////
	//	Plugin properties	//
	//////////////////////////

	//	Define allowed chart types
	var supportedChartTypes = ["chart/column","chart/line","chart/bar","chart/area","chart/scatter"];
	
	//	Regression options
	var none = 'none';

	//var regressionOptions = [none,'linear','exponential','polynomial','logarithmic','loess'];
	var regressionOptions = [none,'linear','exponential','logarithmic'];	
	var regressionProperty = 'regressionType';	

	//	Prediciton methods for how many
	var calendarOption = 'calendar end';
	//var predictionMethods = [none, calendarOption,'rolling 3', 'rolling 6', 'rolling 12'];
	var predictionMethods = [
		{
			label: none,			
		},
		{
			label: calendarOption
		},
		{
			label: 'Next 3',
			numPeriods: 3
		},
		{
			label: 'Next 6',
			numPeriods: 6
		},
		{
			label: 'Next 12',
			numPeriods: 12
		},
	]

	//	Default settings
	var defaultSettings = {
		enabled : false,
		predictionMethod: calendarOption,
        periodsAdded:false,
	}

	//	Define future periods
	var yearsToPredict = 5,
		weeksToPredict = 5;

	//	Get the date formatter
	prism.dateFormatter = $filter("levelDate");		


	//////////////////////////
	//	Utility Functions	//
	//////////////////////////

	//	Function to determine if this chart type is supported
	function widgetIsSupported(type){
		if (supportedChartTypes.indexOf(type) >= 0) {
			return true;
		} else {
			return false;
		}
	}

	//	function to find the value items from a widget
	function getItems(widget,section) {
		var valueItems = $.grep(widget.metadata.panels, function(w){
			return w.title===section;
		})[0];
		return valueItems;
	}

	//	function to find the value items from a widget
	function getDim(widget) {
		//	Get the first panel
		var firstPanel = widget.metadata.panels[0];
		
		//	Get only the enabled items
		var enabledItems = $.grep(firstPanel.items, function(w) {
			return (typeof w.disabled === 'undefined') || (w.disabled === false);
		})
		
		//	Return the first enabled item
		return enabledItems[0];		
	}

	//	Function to convert the data from its existing structure to the following: [ [dim, value], [dim, value] ]
	function fixData(data){

		//	Create new array to return
        var ret = [data.length];

        //	Loop through the data and structure each object an array of values
        for (var i = 0 ; i < data.length ; i++){
            ret[i] = [2];
            ret[i][0] = data[i].selectionData[0];
            ret[i][1] = data[i].y;
        }

        //	Return new array data
        return ret;
    }

    //	Function to convert the data from an array, back to an objct
    function fixDataReversed(data){

    	//	Create a new array to return
        var ret = [data.length];

        //	Loop through the data and convert each array to an object
        for (var i = 0 ; i < data.length ; i++){
            ret[i] = {selectionData : [data[i][0]], y :  data[i][1]};
        }

        //	Return the converted data
        return ret;
    }


    //////////////////////////////
	//	Prediction Functions	//
	//////////////////////////////

	//  Function to predict future values
    function _prediction(data,options,regressionType) {        
        
        //  Do we need to predict?
        if (!options.enabled) {
            //  No prediction
            return data;
        } else {            
            //  Predict future values
            var equation = data.equation;
            var m = data.equation[0];
            var b = data.equation[1];
            var numPoints = data.points.length;
            
            //  Get the last available date
            var lastPoint = data.points[numPoints-1];
            var lastDate = lastPoint[0];
            var lastValue = lastPoint[1];

            //  Calculate future time periods
            var forecastArray = options.projectedPeriods;
            data.points = data.points.concat(forecastArray);            
            
            //  Calcualte future values           
            for (var i=numPoints; i<data.points.length; i++){
                if (regressionType === 'linear'){
                    //  Linear Regression: Y = m * X + b
                    var forecastValue = m * i + b;
                } else if (regressionType === 'exponential') {
                    //  Exponential Regression
                    //var forecastValue = m * Math.pow(Math.E, b * i);
                    var forecastValue = m * Math.pow(Math.E, b * data.points[i][0]);
                } else if (regressionType === 'logarithmic') {
                    //  Logarithmic Regression                    
                    var forecastValue = m + (b * Math.log(data.points[i][0]));
                }
                //  Add value to the array
                data.points[i][1] = forecastValue;
            }

            return data;
        }
    }

	/**
     * Code extracted from https://github.com/Tom-Alexander/regression-js/
     */
    function _exponential(data) {
        var sum = [0, 0, 0, 0, 0, 0], n = 0, results = [];

        for (len = data.length; n < len; n++) {
          if (data[n]['x']) {
            data[n][0] = data[n]['x'];
            data[n][1] = data[n]['y'];
          }
          if (data[n][1]) {
            sum[0] += data[n][0]; // X
            sum[1] += data[n][1]; // Y
            sum[2] += data[n][0] * data[n][0] * data[n][1]; // XXY
            sum[3] += data[n][1] * Math.log(data[n][1]); // Y Log Y 
            sum[4] += data[n][0] * data[n][1] * Math.log(data[n][1]); //YY Log Y
            sum[5] += data[n][0] * data[n][1]; //XY
          }
        }

        var denominator = (sum[1] * sum[2] - sum[5] * sum[5]);
        var A = Math.pow(Math.E, (sum[2] * sum[3] - sum[5] * sum[4]) / denominator);
        var B = (sum[1] * sum[4] - sum[5] * sum[3]) / denominator;
        
        for (var i = 0, len = data.length; i < len; i++) {
            var coordinate = [data[i][0], A * Math.pow(Math.E, B * data[i][0])];
            results.push(coordinate);
        }

        results.sort(function(a,b){
           if(a[0] > b[0]){ return 1}
            if(a[0] < b[0]){ return -1}
              return 0;
        });

        var string = 'y = ' + Math.round(A*100) / 100 + 'e^(' + Math.round(B*100) / 100 + 'x)';

        return {equation: [A, B], points: results, string: string};
    } 

    /**
     * Code extracted from https://github.com/Tom-Alexander/regression-js/
     * Human readable formulas: 
     * 
     *              N * Σ(XY) - Σ(X) 
     * intercept = ---------------------
     *              N * Σ(X^2) - Σ(X)^2
     * 
     * correlation = N * Σ(XY) - Σ(X) * Σ (Y) / √ (  N * Σ(X^2) - Σ(X) ) * ( N * Σ(Y^2) - Σ(Y)^2 ) ) )
     * 
     */
    function _linear(data) {
        var sum = [0, 0, 0, 0, 0], n = 0, results = [], N = data.length;

        for (; n < data.length; n++) {
          if (data[n]['x']) {
            data[n][0] = data[n]['x'];
            data[n][1] = data[n]['y'];
          }
          if (data[n][1]) {            
            sum[0] += n; //Σ(X) 
            sum[1] += data[n][1]; //Σ(Y)
            sum[2] += n * n; //Σ(X^2)
            sum[3] += n * data[n][1]; //Σ(XY)
            sum[4] += data[n][1] * data[n][1]; //Σ(Y^2)
          }
        }
        

        var gradient = (N * sum[3] - sum[0] * sum[1]) / (N * sum[2] - sum[0] * sum[0]);
        var intercept = (sum[1] / N) - (gradient * sum[0]) / N;
        //var correlation = (n * sum[3] - sum[0] * sum[1]) / Math.sqrt((n * sum[2] - sum[0] * sum[0]) * (n * sum[4] - sum[1] * sum[1]));
        
        for (var i = 0, len = data.length; i < len; i++) {
            var coordinate = [data[i][0], i * gradient + intercept];
            results.push(coordinate);
        }

        results.sort(function(a,b){
           if(a[0] > b[0]){ return 1}
            if(a[0] < b[0]){ return -1}
              return 0;
        });

        var string = 'y = ' + Math.round(gradient*100) / 100 + 'x + ' + Math.round(intercept*100) / 100;
        return {equation: [gradient, intercept], points: results, string: string};
    }
    
    /**
     *  Code extracted from https://github.com/Tom-Alexander/regression-js/
     */
    function _logarithmic(data) {
        var sum = [0, 0, 0, 0], n = 0, results = [],mean = 0 ;
        

        for (len = data.length; n < len; n++) {
          if (data[n]['x']) {
            data[n][0] = data[n]['x'];
            data[n][1] = data[n]['y'];
          }
          if (data[n][1]) {
            sum[0] += Math.log(data[n][0]);
            sum[1] += data[n][1] * Math.log(data[n][0]);
            sum[2] += data[n][1];
            sum[3] += Math.pow(Math.log(data[n][0]), 2);
          }
        }
        
        var B = (n * sum[1] - sum[2] * sum[0]) / (n * sum[3] - sum[0] * sum[0]);
        var A = (sum[2] - B * sum[0]) / n;

        for (var i = 0, len = data.length; i < len; i++) {
            var coordinate = [data[i][0], A + B * Math.log(data[i][0])];
            results.push(coordinate);
        }

        results.sort(function(a,b){
           if(a[0] > b[0]){ return 1}
            if(a[0] < b[0]){ return -1}
              return 0;
        });

        var string = 'y = ' + Math.round(A*100) / 100 + ' + ' + Math.round(B*100) / 100 + ' ln(x)';
        
        return {equation: [A, B], points: results, string: string};
    }
    
    /**
     * Code extracted from https://github.com/Tom-Alexander/regression-js/
     */
    function _power(data) {
        var sum = [0, 0, 0, 0], n = 0, results = [];

        for (len = data.length; n < len; n++) {
          if (data[n]['x']) {
            data[n][0] = data[n]['x'];
            data[n][1] = data[n]['y'];
          }
          if (data[n][1]) {
            sum[0] += Math.log(data[n][0]);
            sum[1] += Math.log(data[n][1]) * Math.log(data[n][0]);
            sum[2] += Math.log(data[n][1]);
            sum[3] += Math.pow(Math.log(data[n][0]), 2);
          }
        }

        var B = (n * sum[1] - sum[2] * sum[0]) / (n * sum[3] - sum[0] * sum[0]);
        var A = Math.pow(Math.E, (sum[2] - B * sum[0]) / n);

        for (var i = 0, len = data.length; i < len; i++) {
            var coordinate = [data[i][0], A * Math.pow(data[i][0] , B)];
            results.push(coordinate);
        }

        results.sort(function(a,b){
           if(a[0] > b[0]){ return 1}
            if(a[0] < b[0]){ return -1}
              return 0;
        });

        var string = 'y = ' + Math.round(A*100) / 100 + 'x^' + Math.round(B*100) / 100;

        return {equation: [A, B], points: results, string: string};
    }
    
    /**
     * Code extracted from https://github.com/Tom-Alexander/regression-js/
     */
    function _polynomial(data, order) {
        if(typeof order == 'undefined'){
            order =2;
        }
        var lhs = [], rhs = [], results = [], a = 0, b = 0, i = 0, k = order + 1;

        for (; i < k; i++) {
            for (var l = 0, len = data.length; l < len; l++) {
                if (data[l]['x']) {
                    data[l][0] = data[l]['x'];
                    data[l][1] = data[l]['y'];
                }
                if (data[l][1]) {
                    a += Math.pow(data[l][0], i) * data[l][1];
                }
            }
            lhs.push(a), a = 0;
            var c = [];
            for (var j = 0; j < k; j++) {
                for (var l = 0, len = data.length; l < len; l++) {
                    if (data[l][1]) {
                        b += Math.pow(data[l][0], i + j);
                    }
                }
                c.push(b), b = 0;
            }
            rhs.push(c);
        }
        rhs.push(lhs);

        var equation = gaussianElimination(rhs, k);

        for (var i = 0, len = data.length; i < len; i++) {
            var answer = 0;
            for (var w = 0; w < equation.length; w++) {
                answer += equation[w] * Math.pow(data[i][0], w);
            }
            results.push([data[i][0], answer]);
        }

        results.sort(function(a,b){
           if(a[0] > b[0]){ return 1}
            if(a[0] < b[0]){ return -1}
              return 0;
        });

        var string = 'y = ';

        for(var i = equation.length-1; i >= 0; i--){
            if(i > 1) string += Math.round(equation[i]*100) / 100 + 'x^' + i + ' + ';
            else if (i == 1) string += Math.round(equation[i]*100) / 100 + 'x' + ' + ';
            else string += Math.round(equation[i]*100) / 100;
        }

        return {equation: equation, points: results, string: string};
    }
    
    /**
     * @author: Ignacio Vazquez
     * Based on 
     * - http://commons.apache.org/proper/commons-math/download_math.cgi LoesInterpolator.java
     * - https://gist.github.com/avibryant/1151823
     */
    function _loess (data, bandwidth) {
        var bandwidth = bandwidth || 0.25 ;
        
        var xval = data.map(function(pair){return pair[0]});
        var distinctX =  array_unique(xval) ;
        if (  2 / distinctX.length  > bandwidth ) {
            bandwidth = Math.min( 2 / distinctX.length, 1 );
            console.warn("updated bandwith to "+ bandwidth);
        }
        
        var yval = data.map(function(pair){return pair[1]});
        
        function array_unique(values) {
            var o = {}, i, l = values.length, r = [];
            for(i=0; i<l;i+=1) o[values[i]] = values[i];
            for(i in o) r.push(o[i]);
            return r;
        }
        
        function tricube(x) {
            var tmp = 1 - x * x * x;
            return tmp * tmp * tmp;
        }

        var res = [];

        var left = 0;
        var right = Math.floor(bandwidth * xval.length) - 1;

        for(var i in xval)
        {
            var x = xval[i];
    
            if (i > 0) {
                if (right < xval.length - 1 &&
                        xval[right+1] - xval[i] < xval[i] - xval[left]) {
                    left++;
                    right++;
                }
            }
            //console.debug("left: "+left  + " right: " + right );
            var edge;
            if (xval[i] - xval[left] > xval[right] - xval[i])
                edge = left;
            else
                edge = right;
            var denom = Math.abs(1.0 / (xval[edge] - x));
            var sumWeights = 0;
            var sumX = 0, sumXSquared = 0, sumY = 0, sumXY = 0;

            var k = left;
            while(k <= right)
            {
                var xk = xval[k];
                var yk = yval[k];
                var dist;
                if (k < i) {
                    dist = (x - xk);
                } else {
                    dist = (xk - x);
                }
                var w = tricube(dist * denom);
                var xkw = xk * w;
                sumWeights += w;
                sumX += xkw;
                sumXSquared += xk * xkw;
                sumY += yk * w;
                sumXY += yk * xkw;
                k++;
            }

            var meanX = sumX / sumWeights;
            //console.debug(meanX);
            var meanY = sumY / sumWeights;
            var meanXY = sumXY / sumWeights;
            var meanXSquared = sumXSquared / sumWeights;

            var beta;
            if (meanXSquared == meanX * meanX)
                beta = 0;
            else
                beta = (meanXY - meanX * meanY) / (meanXSquared - meanX * meanX);

            var alpha = meanY - beta * meanX;
            res[i] = beta * x + alpha;
        }        
        return { 
            equation: "" , 
            points: xval.map(function(x,i){return [x, res[i]]}), 
            string:""
        } ;
    }
    
    
    /**
     * Code extracted from https://github.com/Tom-Alexander/regression-js/
     */
    function  gaussianElimination(a, o) {
        var i = 0, j = 0, k = 0, maxrow = 0, tmp = 0, n = a.length - 1, x = new Array(o);
        for (i = 0; i < n; i++) {
           maxrow = i;
           for (j = i + 1; j < n; j++) {
              if (Math.abs(a[i][j]) > Math.abs(a[i][maxrow]))
                 maxrow = j;
           }
           for (k = i; k < n + 1; k++) {
              tmp = a[k][i];
              a[k][i] = a[k][maxrow];
              a[k][maxrow] = tmp;
           }
           for (j = i + 1; j < n; j++) {
              for (k = n; k >= i; k--) {
                 a[k][j] -= a[k][i] * a[i][j] / a[i][i];
              }
           }
        }
        for (j = n - 1; j >= 0; j--) {
           tmp = 0;
           for (k = j + 1; k < n; k++)
              tmp += a[k][j] * x[k];
           x[j] = (a[n][j] - tmp) / a[j][j];
        }
        return (x);
     }
    
    /**
     * @author Ignacio Vazquez 
     * See http://en.wikipedia.org/wiki/Coefficient_of_determination for theaorical details 
     */
    function coefficientOfDetermination (data, pred ) {
        
        var i = SSE = SSYY =  mean = 0;

        // Calc the mean
        for (i = 0 ; i < data.length ; i++ ){
            mean +=  data[i][1];
        }
        mean = mean / data.length;
        
        // Calc the coefficent of determination 
        for (i = 0 ; i < data.length ; i++ ){
            SSYY +=  Math.pow( data[i][1] -  pred[i][1] , 2) ;
            SSE +=  Math.pow( data[i][1] -  mean , 2) ;
        }
        
        return  1 - ( SSYY / SSE)  ;
    }


	//////////////////////////////
	//	Regression Functions	//
	//////////////////////////////

	//	Function to kick off a regression analysis
	function startRegression(widget,args) {		

		//	Get a list of the value items
		var valueItems = getItems(widget,'values');
		var category = getDim(widget);
		
		if (valueItems) {
			//	Loop through each series
			for (var i=0; i<valueItems.items.length; i++){

				//	Should regression be calculated for this series?
				var regressionEnabled = (valueItems.items[i][regressionProperty]) && (valueItems.items[i][regressionProperty] !== none);
				if (regressionEnabled) {

					//	Get saved settings
					var settings = widget.options.regression;
					if (typeof settings === 'undefined'){
						//	Create defaults, if not defined
						settings = defaultSettings;
					}

					//	Enable regression for this series					
					args.result.series[i].regression = true;

					//	Define the future periods to predict out
					var predictionOptions = definePredictionOptions(args.result,args.result.series[i],category,settings);
					
					//	Set the options for this series		
					args.result.series[i].regressionSettings = {
						type: valueItems.items[i][regressionProperty],
						color: valueItems.items[i].format.color.color,
						dashStyle: 'dash',
						name: valueItems.items[i].jaql.title,
						projections: predictionOptions,
						style: widget.style
					};

					//	Update the chart to show future points		
					addFuturePeriodsAxisLabels(args.result,predictionOptions,i);					

					//	Calculate trendline, and replace the old series				
					var newSeries = calculateTrend(args.result.series[i]);					
					args.result.series[i] = newSeries;
				}
			}				
		}		
	}

	//	Figure out how many periods to predict out
	function definePredictionOptions(chart,series,dim,settings){

		//	Create options object
		var options = {
			enabled: false,
			predictionMethod: settings.predictionMethod
		};

		//	Is the axis a datetime?
		if (dim.jaql.datatype !== 'datetime' || settings.predictionMethod === none) {

			//	No need to predict into the future, as the axis is not a datetime
			return options;

		} else {

			//	Need to predict into the future
			options.enabled = true;

			//	Get the formatting options
			options.level = dim.jaql.level;
			options.mask = dim.format.mask[dim.jaql.level];

			//	Figure out how far to predict ahead
			var maxDate = series.data[series.data.length-1].selectionData[0];
			var futurePeriods = calculateFuturePeriods(maxDate,options);			
			options.projectedPeriods = futurePeriods;			

		}
		
		return options;
	}

	//  Function to determine the # of future periods
    function calculateFuturePeriods(maxDate,options) {

    	//	Get parameters
    	var predictionMethod = options.predictionMethod;
    	var extraPeriods = $.grep(predictionMethods, function(w) {
    		return w.label === predictionMethod;
    	})[0].numPeriods
    	var level = options.level;

        //  Create an array to send back
        var datesArray = [];

        //	Get the max date info        
        var lastMonth = maxDate.getMonth();
        var lastYear = maxDate.getFullYear();  
        var lastDay = maxDate.getDate();  
        var lastWeek = parseInt(prism.dateFormatter(maxDate,'w'));      

        //  Check the level
        if (level === 'days') {

        	//  Figure out how many days to predict
        	var endDate = new Date(lastYear,lastMonth+1,0);
        	var endDay = endDate.getDate();
        	var numDays = extraPeriods ? extraPeriods : endDay - lastDay;        	

    		//	Predict out X number of days
    		for (var i=1; i<=numDays; i++) {

    			//  Create the new date
                var newDate = new Date(lastYear,lastMonth,lastDay+i);

                //  Create the new data object
                var newDatapoint = [newDate,null];

                //  Add to forecast series
                datesArray.push(newDatapoint);
    		}

        } else if (level === 'weeks') {

        	//  Figure out how many months to predict
        	var numWeeks = extraPeriods ? extraPeriods : 52 - lastWeek;

            //  Loop through all remaining dates
            for (var i=1; i<=numWeeks; i++) {

                //  Create the new date, add 7 days
                var newDate = new Date(lastYear,lastMonth,lastDay);
                newDate.setTime( newDate.getTime() + (i*7) * 86400000 );                

                //  Create the new data object
                var newDatapoint = [newDate,null];

                //  Add to forecast series
                datesArray.push(newDatapoint);
            }
        
        } else if (level === 'months') {

        	//  Figure out how many months to predict
        	var numMonths = extraPeriods ? extraPeriods : 12 - (lastMonth+1);

            //  Loop through all remaining dates
            for (var i=1; i<=numMonths; i++) {

                //  Create the new date
                var newDate = new Date(lastYear,lastMonth+i,1);

                //  Create the new data object
                var newDatapoint = [newDate,null];

                //  Add to forecast series
                datesArray.push(newDatapoint);
            }
        
        } else if (level === 'quarters') {

        	//  Figure out how many quarters to predict		
	        var currentQuarter = Math.round((lastMonth+1)/4) + 1;
	        var numQuarters = extraPeriods ? extraPeriods : 4;

        	//	Loop through all remaining dates
        	for (var i=1; i<=numQuarters; i++) {

        		//	Calculate new quarter month index
        		var quarterMonthIndex = (i * 3);        		
        		
        		//  Create the new date
                var newDate = new Date(lastYear,lastMonth + quarterMonthIndex,1);

                //  Create the new data object
                var newDatapoint = [newDate,null];

                //  Add to forecast series
                datesArray.push(newDatapoint);
        	}
	        
        }  else if (level === 'years') {

        	//  Figure out how many years to predict		
        	var numYears = extraPeriods ? extraPeriods : 5;

        	//	Loop through all remaining dates
        	for (var i=1; i<=numYears; i++) {

        		//  Create the new date
                var newDate = new Date(lastYear+i,1,1);

                //  Create the new data object
                var newDatapoint = [newDate,null];

                //  Add to forecast series
                datesArray.push(newDatapoint);
        	}
        } 

        return datesArray;
    }

    //	Function to add axis labels
    function addFuturePeriodsAxisLabels(chart,options,index){

    	try {
	    	//	Do we need to run this? true by default
	    	var shouldRun = true;            

	    	//	Regression not enabled, mark flag as false
	    	if (options.enabled === false) {
	    		shouldRun = false;
	    	}

            //  Have we already added new dates?
            if (options.periodsAdded) {
                shouldRun = false;
            }

			//	axis labels already added, mark flag as false		
			var chartLength = chart.xAxis.categories.length;
			var dataLength = chart.series[index].data.length + options.projectedPeriods.length;
	    	if (chartLength === dataLength) {
	    		shouldRun = false;
	    	}

	    	//	If this should still run, add axis labels
	    	if (shouldRun){
	    		
	    		//	Get the formatting mask
	    		var mask = options.mask;
	    		
	    		//	Loop through each point, and create a label
	    		$.each(options.projectedPeriods, function(){
	    			//	Parse the date
    				//var thisDate = Date.parseDate(this[0]);    				
    				var thisDate = Date.parseDate ? Date.parseDate(this[0]) : this[0];
    				//	Create a formatted label
	    			var label = prism.dateFormatter(thisDate,mask)
	    			//	Add to the axis categories
	    			chart.xAxis.categories.push(label);
	    		})

                //  specify that the periods have been added
                options.periodsAdded = true;
	    		
	    	}
    	} catch(e) {
    		console.log(e);
    	}
    }

	//	Function to handle the prediction
	function calculateTrend(oldSeries) {	

		//	Reformat the data
        var data = fixData(oldSeries.data);        
        var dataOriginal = data.slice(0);

        //	Make sure the regression settings have valid options
        oldSeries.regressionSettings =  oldSeries.regressionSettings || {} ;
        oldSeries.regressionSettings.tooltip = oldSeries.regressionSettings.tooltip || {} ;
        oldSeries.regressionSettings.dashStyle = oldSeries.regressionSettings.dashStyle || 'solid';
        oldSeries.regressionSettings.projections = oldSeries.regressionSettings.projections || {};

        //	Figure out the regression type
        var regressionType = oldSeries.regressionSettings.type || "linear" ;
        var regression; 

        //	Create a new data series to show the trend
        var newSeries = {
                data:[],
                color: oldSeries.color ,
                yAxis: oldSeries.yAxis ,
                lineWidth: 2,
                marker: {enabled: false} ,
                isRegressionLine: true,
                type: oldSeries.regressionSettings.linetype || 'spline',
                name: oldSeries.regressionSettings.name || "Equation: %eq",
                color: oldSeries.regressionSettings.color || '',
                dashStyle: oldSeries.regressionSettings.dashStyle || 'solid',
                tooltip:{ 
                    valueSuffix : oldSeries.regressionSettings.tooltip.valueSuffix || ' '
            	}
        };

        //  Figure out which regression type to use             
        if (regressionType == "linear") {
            regression = _linear(data) ;
            newSeries.type = "line";
        }else if (regressionType == "exponential") {
            regression = _exponential(data)
        }                                
        else if (regressionType == "polynomial"){  
            var order = s.regressionSettings.order || 2
            regression = _polynomial(data, order) ;
        }else if (regressionType == "logarithmic"){
            regression = _logarithmic(data) ;
        }else if (regressionType == "loess"){
            var loessSmooth = s.regressionSettings.loessSmooth || 25
            regression = _loess(data, loessSmooth/100) ;
        }else {
            console.error("Invalid regression type: " , regressionType) ;
            return;
        }
        
        //  Predict future data points?  
        data = _prediction(regression, oldSeries.regressionSettings.projections,regressionType);
        
        //	Figure out the r^2 and r values
        regression.rSquared =  coefficientOfDetermination(dataOriginal, regression.points).toFixed(2);
        regression.rValue = Math.sqrt(regression.rSquared,2).toFixed(2) ;
		newSeries.name = newSeries.name.replace("%r2",regression.rSquared);
        newSeries.name = newSeries.name.replace("%r",regression.rValue);
        newSeries.name = newSeries.name.replace("%eq",regression.string);
        
        //	Return the data back to object format
        newSeries.data = fixDataReversed(regression.points);
        
        //	Save the regression results to the data series
        newSeries.regressionOutputs = regression ;

        //	Add the mask and y-axis
        newSeries.mask = oldSeries.mask;
        newSeries.yAxis = oldSeries.yAxis;                
                
        //  Set the user-defined min/max 
        /*
        if (oldSeries.regressionSettings) {    
            var styler = oldSeries.regressionSettings.style;
            arguments[1].yAxis[0].max = styler.yAxis.max;
            arguments[1].yAxis[0].min = styler.yAxis.min;            
        }*/
        
        //	return the trendline series
        return newSeries;   
    }

	//////////////////////////////////
	//	Initialization Functions	//
	//////////////////////////////////

	//	Function to determine if the regression option should be added
	function canEnableRegression(e,args,headerMenuCaption,menuType){
		
		//	Assume true by default
		var result = true;				
		try {
			//	Has the menu been added already?			
			$.each(args.settings.items, function(){
				if (this.caption === headerMenuCaption) {
					result = false;
				}
			})

			//	Only show this in the widget editor
			var widgetEditorOpen = (prism.$ngscope.appstate == "widget");
			if (!widgetEditorOpen) {
				result = false;
			}
			
			// find the widget
			var widget = e.currentScope.widget;
			if(widget == undefined){
				result = false;
			}	

			// Widget must be an allowed chart
			if(!widgetIsSupported(widget.type)){
				result = false;
			}

			//	Check where the user clicked
			var target = args.ui.target;            
			if (menuType === 'measure') {
				//	Make sure the user clicked on a measures menu
				if (args.settings.name !== "widget-metadataitem") {
					result = false;
				}
				//	Make sure the settings menu is for a value
				if (args.settings.item.$$panel.name !== "values") {
					result = false;
				}
			} else if (menuType === 'settings') {
				//	Make sure the user clicked on a settings menu
				if (args.ui.css !== "w-menu-host") {
					result = false;
				}
			}

			
		} catch(e) {
			result = false;
		}

		//	Return result
		return result;
	}

	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		//	Hooking to ready/destroyed events
		args.widget.on("destroyed", onWidgetDestroyed);
		//	Should we run a quadrant analysis?
		var shouldInit = widgetIsSupported(args.widget.type);
		//	Add hook for the quadrant analysis
		if (shouldInit) {
			args.widget.on("processresult", startRegression);
		}
	}

	// unregistering widget events
	function onWidgetDestroyed(widget, args) {
		widget.off("destroyed", onWidgetDestroyed);
	}

	//	Create Options for the quadrant analysis
	prism.on("beforemenu", function (e, args) {

		//	Define the menu label
		var headerMenuCaption = "Regression Line";

		//	Can we show the regression options?			
		var addMeasureMenuItem = canEnableRegression(e,args,headerMenuCaption,'measure');
		var addSettingsMenuItem = canEnableRegression(e,args,headerMenuCaption,'settings');		

		//	Add the option to enable the regression line
		if (addMeasureMenuItem) {			
			
			//	Get the widget
			var widget = args.settings.item.$$panel.$$widget;	

			//	Look for a saved regression property
			var savedRegressionType = args.settings.item[regressionProperty] ? args.settings.item[regressionProperty] : none;

			//	Function to create the measure sub-menu items				
			var regressionTypeItems = function(panelItem) {
				//	Create an array to hold the list
				var items = [];

				//	Get the setting for this
				var selectedType = savedRegressionType;
				
				//	Function the runs when an item is picked
				var itemPicked = function(){						
					//	Update the axis setting
					this.panelItem[regressionProperty] = this.caption;
					//	Redraw the widget
					widget.redraw();						
				}

				//	Loop through each regression option and add sub-menu item
				$.each(regressionOptions,function(){
					
					//	Get the option as a string
					var option = this.toString();

					//	Create the menu option
					var item = {
						caption: option,
						checked: (option === selectedType),
						size: 'xl',
						type: 'check',
						panelItem: panelItem,
						execute: itemPicked
					}

					//	Add to the list
					items.push(item);
				})				

				//	Return the list of items				
				return items;
			}

			//	Create menu for picking the regression measure
			var regressionTypes = {
				caption: "Regression",
				items: regressionTypeItems(args.settings.item)
			};

			//	Add options to the menu			
			args.settings.items.push(regressionTypes);	
		}

		//	Add the options in the settings menu
		if (addSettingsMenuItem) {

			//	Get the widget
			var widget = e.currentScope.widget;

			//	Get saved settings
			var settings = widget.options.regression;
			if (typeof settings === 'undefined'){
				//	Create defaults, if not defined
				settings = defaultSettings;
			}

			//	Function to create the axis menu items				
			var predictionOptionItems = function(settings) {
				//	Create an array to hold the list
				var items = [];

				//	Get the setting for this				
				var selectedMethod = settings.predictionMethod;
				
				//	Function the runs when an item is picked
				var itemPicked = function(){						
					//	Update the axis setting
					this.settings.predictionMethod = this.caption;
					widget.options.regression = this.settings;
					//	Redraw the widget
					widget.redraw();						
				}

				//	Add all prediction methods
				$.each(predictionMethods,function(){
					
					//	Get the option as a string
					var option = this.label;

					//	Create the menu option
					var item = {
						caption: option,
						checked: (option === selectedMethod),
						size: 'xl',
						type: 'check',
						widget: widget,
						settings: settings,						
						execute: itemPicked
					}

					//	Add to the list
					items.push(item);
				})				

				//	Return the list of items				
				return items;
			}

			//	Create header
			var header = {
				caption: headerMenuCaption,
				type: "header",
				disabled: false
			};			

			//	Create menu for picking the regression measure
			var precitionMenuOptions = {
				caption: "Prediction Options",
				items: predictionOptionItems(settings)
			};

			//	Add options to the menu
			args.settings.items.push(header);			
			args.settings.items.push(precitionMenuOptions);

		}
	});

}]);