// Un-zip this folder into C:\Program Files\Sisense\PrismWeb\plugins\ and refresh the app.


/*
* Conditional coloring of cells.
* Put this into widget's script editor window
*/
widget.event('createdRow').on('createdRow', function(widget, args) {

	var
		row = args.row, // <TR> DOM element from table
		data = args.data, // array with data displayed in current row (type string)
		dataIndex = args.dataIndex // index of row in current render cycle
		;

    // small example: we have two items in Columns sub-panel like Country and Cost
	if (parseFloat(data[1], 10) > 1)
		$(row).find('td:eq(1)').css('background', '#ff8899');
	else
		$(row).find('td:eq(1)').css('background', '#99FF88');

});