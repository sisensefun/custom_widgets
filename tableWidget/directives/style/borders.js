// markers
mod.directive( 'borders', [
    'plugin-tableWidget.factories.editorStyleFactory',

    function (editorStyleFactory) {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
                selectedBorders: '=',
                onChange: '&'
            },
            templateUrl: "/plugins/tableWidget/views/style/borders.html?r=" + Math.random(),
            link: function (scope, element, attrs) {
                var style = scope.$root.widget.style;

                /**
                 * variables
                 */
                scope.bordersOptions = editorStyleFactory.BORDERS_OPTIONS_array;
                scope.selectedBorders = (function() {
                    for (var name in style) {
                        if (~name.indexOf( "borders/") && style[name]) {
                            return name;
                        }
                    }
                }());

                /**
                 * public methods
                 */
                scope.selectBorders = function( borders) {
                    scope.selectedBorders = borders.value;

                    for (var name in style) {
                        if (~name.indexOf( "borders/")) {
                            style[name] = false;
                        }
                    }

                    style[borders.value] = true;
                    scope.onChange();
                };

                scope.getSelectedBordersName = function () {
                    var
                        selectedBorders = _.findWhere(
                            scope.bordersOptions,
                            { value: scope.selectedBorders }
                        )
                    ;

                    return selectedBorders && selectedBorders.name;
                };

                scope.mouseEnter = function (hoveredItem) {
                    if (scope.selectedBorders != hoveredItem.value){
                        scope.hoveredBorders = hoveredItem.name;
                    } else {
                        scope.hoveredBorders = null;
                    }
                };

                scope.mouseLeave = function () {
                    scope.hoveredBorders = null;
                };
            }
        };
    }
]);
