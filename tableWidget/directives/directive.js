/**
 * Directive for widget
 *
 * @author Volodymyr Sheremeta <volodymyr.sh@sisense.com>
 */
mod.directive('tablewidgetagg', [
    'ux-controls.services.$dom',
    '$filter',
    '$timeout',
    'plugin-tableWidget.services.dataService',
    'plugin-tableWidget.services.sortingService',
    'dashboard-base.services.$query',
    '$rootScope',

    function ($dom, $filter, $timeout, $dataService, $sortingService, $query, $rootScope) {
        return {

            priority: 0,
            replace: false,
            templateUrl: "/plugins/tableWidget/views/widget-template.html?r=" + Math.random(),
            transclude: false,
            restrict: 'E',
            link: function ($scope, element, attrs) {

                var
                // translations
                    _translate = $filter('translate'),
                    _prefix = 'TABLE_SIGNS.',
                    $translate = function (id) {return _translate(_prefix + id);},
                    NO_DATA = $translate('NO_DATA'),
                    PAGE_INFO = $translate('PAGE_MODE_INFO'),

                    widget = $scope.widget,
                    style = $scope.widget.style,
                    tablewidget = new NewTableWidget(),
                    designStyles = [
                        'borders/all',
                        'borders/grid',
                        'borders/rows',
                        'borders/columns',

                        'colors/headers',
                        'colors/rows',
                        'colors/columns'
                    ],
                    cachedStyle = {},
                    cachedData = [],
                    numberFilter = $filter('numeric'),
                    datetimeFilter = $filter('levelDate')
                    ;

                // rendering handler
                function onRender(widgetSender, args) {
                    $$.debug.log('tablewidget :: onRender');

                    var
                        numberFormatters = [],
                        datetimeFormatters = [],
                        headers = [],
                        isStyleChanged = false
                        ;

                    if (compareStyles(cachedStyle, widget.style)) {
                        isStyleChanged = !_.isEmpty(cachedStyle);

                        designStyles.forEach(function (item) {
                            cachedStyle[item] = widget.style[item];
                        });
                    }

                    if (!isStyleChanged) {
                        widget.metadata.panel("columns").items.forEach(function (item) {

                            if (item.disabled) return;

                            headers.push({
                                title: item.jaql.title,
                                dim: item.jaql.dim,
                                agg: item.jaql.agg,
                                level: item.jaql.level,
                                formula: item.jaql.formula,
                                datatype: item.jaql.datatype
                            });

                            var mask = defined(item, 'format.mask') ? item.format.mask : widget.manifest.mask.measure();

                            numberFormatters.push(function (number) {
                                return numberFilter(number, mask);
                            });

                            datetimeFormatters.push(function (date) {
                                return datetimeFilter(date, mask[item.jaql.level]);
                            });
                        });
                    }

                    widget.style.pageSize = parseInt(widget.style.pageSize, 10);

                    tablewidget.render({
                        isData: defined(widget, 'queryResult.$$rows.length') && (widget.queryResult.$$rows.length > 0),
                        isStyleChanged: isStyleChanged,
                        // if the widget's 'render' event reason is 'widgetrefresh' or 'refresh' -
                        // then it was triggered after the filters change
                        isFiltersChanged: args.reason == "refresh" || args.reason == "widgetrefresh",
                        // saving widget style and sorting properties only in View Mode (when widget is on dashboard)
                        // for users with such privileges (contributors, admin etc)
                        canSaveChanges: !$rootScope.viewonly && $rootScope.appstate !== 'widget',
                        data: $$get(widget, 'queryResult.$$rows'),
                        queryOptions: $$get(widget, 'queryResult.queryOptions'),
                        style: widget.style,
                        headers: headers,
                        cachedData: cachedData,
                        container: $(element).find('.tablewidget-widget-content'),
                        dataService: $dataService,
                        sortingService: $sortingService,
                        widget: widget,
                        bordersStyle: getStyleValue('borders'),
                        query: $query,
                        numberFormatters: numberFormatters,
                        datetimeFormatters: datetimeFormatters,
                        pageModeInfo: PAGE_INFO,
                        noDataSign: NO_DATA,
                        // callback for rows formatting
                        createdRowCallback: defined(widget.$$events['createdRow']) ? createdRowCallback : null,
                        domReadyCallback: function () {
                            widget.trigger("domready");
                        }
                    });

                }

                /**
                 * Callback that is called after each row in rendered into table
                 *
                 * @param {Object} row - <TR> DOM element from table
                 * @param {Array} data - array with data displayed in current row (type string)
                 * @param {Number} dataIndex - index of row in current render cycle
                 */
                function createdRowCallback(row, data, dataIndex) {
                    // trigger custom event from widget's script editor
                    widget.trigger('createdRow', {row: row, data: data, dataIndex: dataIndex})
                }

                /**
                 * Compares styles objects
                 *
                 * @param {Object} cachedStyle
                 * @param {Object} newStyle
                 */
                function compareStyles(cachedStyle, newStyle) {

                    return _.some(designStyles, function (item) {
                        return cachedStyle[item] !== newStyle[item];
                    });
                }

                // container re-size end
                function onResizeEnd(sender, args) {
                    $$.debug.log('tablewidget :: onResizeEnd');

                }

                function getStyleValue(optName, match) {
                    for (var name in style) {
                        if (style.hasOwnProperty(name) && ~name.indexOf(optName + "/") && style[name])
                            return (match ? (name.split("/")[1] == match) : name);
                    }
                }

                // registering widget model events
                widget.on('render', onRender);
                widget.on('resizeend', onResizeEnd);

                // registering scope destroy
                $scope.$on('$destroy', function () {
                    $$.debug.log('tablewidget :: $destroy');

                    // unregistering widget model events
                    widget.off('render', onRender);
                    widget.off('resizeend', onResizeEnd);
                });
            }
        };
    }]);