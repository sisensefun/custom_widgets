﻿/**
 * Factory for widget styles for left design panel
 *
 * @author Volodymyr Sheremeta <volodymyr.sh@sisense.com>
 */
mod.factory('editorStyleFactory', ['$filter', function ($filter) {

    var _translate = $filter('translate'); // Get the "translate" filter
    var _prefix = 'TABLEWIDGET_STYLER.';
    // have to use translation filter, but it doesn't work now.
    // the language file isn't registering properly
    var $translate = function (id) {
        return _translate(_prefix + id);
    };

    // translated captions
    return {
        BORDERS_OPTIONS_array: [
            {
                name: 'Outer and Inner Borders',
                value: 'borders/all'
            },
            {
                name: 'Only Inner Borders',
                value: 'borders/grid'
            },
            {
                name: 'Rows',
                value: 'borders/rows'
            },
            {
                name: 'Columns',
                value: 'borders/columns'
            }
        ],

        COLUMN_WIDTH_OPTIONS_array: [
            {
                name: 'Manual',
                value: 'width/content'
            },
            {
                name: 'Automatic',
                value: 'width/window'
            }
        ],

        COLOR_OPTIONS_array: [
            {
                name: 'Headers',
                value: 'colors/headers'
            },
            {
                name: 'Alternating Rows',
                value: 'colors/rows'
            },
            {
                name: 'Alternating Columns',
                value: 'colors/columns'
            }
        ],

        WORD_WRAP_OPTIONS_array: [
            {
                name: 'Headers',
                value: 'wordwrap/headers'
            },
            {
                name: 'Rows',
                value: 'wordwrap/rows'
            }
        ]
    };
}]);
