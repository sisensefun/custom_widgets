/**
 * Widget loader
 *
 * @author Volodymyr Sheremeta <volodymyr.sh@sisense.com>
 */
var
    maxItemsDimensions = 100,
    defaultLimit = 220,
    ungroupData = true
    ;

prism.registerWidget('tablewidgetagg', {

    name: 'tablewidgetagg',
    family: 'tablewidgetagg',
    title: 'Table With Aggregation',
    priority: 8,
    hideNoResults: false,
    noResultImg: '/plugins/tableWidget/resources/images/table-no-results.svg',
    noResultImgSmall: '/plugins/tableWidget/resources/images/table-no-results-small.svg',
    iconSmall: "/plugins/tableWidget/resources/images/widget-24.png",
    styleEditorTemplate: '/plugins/tableWidget/views/style/style-editor-template.html?t=' + Math.random(),
    style: {
        'borders/all': true,
        'borders/grid': false,
        'borders/rows': false,
        'borders/columns': false,

        'width/content': true,
        'width/window': false,

        'colors/headers': true,
        'colors/rows': true,
        'colors/columns': false,

        'wordwrap/headers': true,
        'wordwrap/rows': false,

        scroll: false,
        pageSize: 25,

        tableState: {} //state of DataTable
    },
    directive: {desktop: "tablewidgetagg"},
    data: {

        selection: [],
        defaultQueryResult: {},
        panels: [
            {
                name: 'columns',
                type: 'default',
                canDisableItems: true,
                itemAdded: function( widget, item) {
                    // removing static color
                    if ($$get(item, "format.color.type") === "color" || $$get(item, "format.color.type") === "static") {
                        delete item.format.color;
                    }

                    if (defined(item, "format.color_bkp")) {
                        prism.$jaql.resetColor(item);
                    }

                    if (prism.$jaql.isMeasure(item) && defined(item, "format.members")) {
                        delete item.format.members;
                    }
                },
                metadata: {
                    types: ["dimensions", "measures"],
                    maxitems: maxItemsDimensions,
                    mixed: true,
                    sortable: {
                        maxitems: 1
                    }
                }
            },
            {
                name: 'filters',
                type: 'filters',
                metadata: {
                    types: ["dimensions", "measures"],
                    maxitems: -1
                }
            }
        ],

        getItems: function (widget) {

            var result = [];

            // categories
            widget.metadata.panel(0).items.copyTo(result);

            return result;
        },

        getMesaureFilteredDimension: function (widget) {

            var p = widget.metadata.panel(0);

            if (p.items.length === 0) {
                return;
            }

            return p.items[0].jaql.dim;
        },

        getMesaureFilteredItem: function (widget) {

            var p = widget.metadata.panel(0);

            if (p.items.length === 0) {
                return;
            }

            return $$.object.clone(p.items[0], true);
        },

        allocatePanel: function (widget, metadataItem) {
            if (!prism.$jaql.isMeasure(metadataItem) && widget.metadata.panel("columns").items.length < maxItemsDimensions) {
                return "columns";
            }
        },

        // returns true/ reason why the given item configuration is not/supported by the widget
        isSupported: function (items) {

            return this.rankMetadata(items, null, null) > -1;
        },

        // ranks the compatibility of the given metadata items with the widget
        rankMetadata: function (items, type, subtype) {
            var a = prism.$jaql.analyze(items);

            // requires 1-20 dimension and no measures
            if (a.dimensions.length > 0 && a.dimensions.length <= maxItemsDimensions && a.measures.length === 0) {

                return 0;
            }

            return -1;
        },

        // populates the metadata items to the widget
        populateMetadata: function (widget, items) {

            var
                $naming = angular.injector(['base', 'eucalyptus']).get('base.services.$naming'),
                a = prism.$jaql.analyze(items)
                ;

            if (items.length > 0) {
                items.forEach(function (item) {

                    // skip filters
                    if (prism.$jaql.isFilter(item)) return;

                    // only dimensions in Column panel
                    if (defined(item, 'format.color')) delete item.format.color;

                    // removing aggregate functions and assigning new title
                    if (defined(item, 'jaql.agg')) {
                        delete item.jaql.agg;
                        item.jaql.title = $naming.toPlainTextTitle(item.jaql);
                    }

                    // skipping fields with formula because it may have few measures inside
                    if (!defined(item, 'jaql.formula')) {
                        widget.metadata.panel("columns").push(item);
                    }
                });
            }

            // from filters
            else if (a.filteredDimensions.length > 0) {

                widget.metadata.panel("columns").push(a.filteredDimensions);
            }

            // allocating filters
            widget.metadata.panel("filters").push(a.filters);
        },

        // builds a jaql query from the given widget
        buildQuery: function (widget) {

            // building jaql query object from widget metadata

            var
                isSorted = false,
                query = {
                    datasource: widget.datasource,
                    metadata: []
                };

            widget.metadata.panel("columns").items.forEach(function (item) {

                if (!item.disabled) {
                    if (!isSorted && item.jaql.sort) {
                        isSorted = true;
                    } else if (isSorted) {
                        delete item.jaql.sort;
                        delete item.jaql.sortDetails;
                    }
                }

                query.metadata.push(item);
            });

            // filters
            widget.metadata.panel("filters").items.forEach(function (item) {

                var filteritem = $$.object.clone(item, true);
                filteritem.panel = "scope";

                query.metadata.push(filteritem);
            });
            // when using $query with offset/limit - make initial query quick
            //query.ungroup = ungroupData;
            query.count = defaultLimit;
            query.offset = 0;

            return query;

        },

        // prepares the widget-specific query result from the given result data-table
        processResult: function (widget, queryResult) {
            queryResult.queryOptions = {
                limit: defaultLimit
                //,ungroup: ungroupData
            };
            return queryResult;
        },

        switch: function (source, target) {
            var isSorted = false;
            // when switching from widget with multiple sorted fields -
            // leave sorted first one and remove sorting on others
            source.metadata.panels.forEach(function (panel) {
                panel.items.forEach(function (item) {
                    if (defined(item, 'jaql.sort')) {

                        if (!isSorted) isSorted = true;
                        else if (isSorted) {
                            delete item.jaql.sort;
                            delete item.jaql.sortDetails;
                        }
                    }
                });
            });
        }
    },

    // declaring default masking
    mask: {
        // default options for number filter
        measure: function (widget, item) {
            return {
                type: "number",
                abbreviations: {
                    t: false,
                    b: false,
                    m: false,
                    k: false
                },
                separated: true,
                decimals: "auto"
            };
        }
    },

    // default widget options
    options: {

        dashboardFiltersMode: "filter",
        triggersDomready: true,
        selector: false,
        dataTypes: {
            dimensions: true,
            measures: false,
            filter: false
        },
        autoUpdateOnEveryChange : true,
        supportsHierarchies: false
    },

    // min/max/default size
    sizing: {

        minHeight: 256,
        maxHeight: 2048,
        minWidth: 256,
        maxWidth: 2048,
        height: 640,
        defaultWidth: 512
    }
});
