/**
 * Controller for style editor template
 *
 * @author Volodymyr Sheremeta <volodymyr.sh@sisense.com>
 */
mod.controller('stylerController', ['$scope', 'plugin-tableWidget.factories.editorStyleFactory',
    function ($scope, editorStyleFactory) {

        $scope.bordersOptions = editorStyleFactory.BORDERS_OPTIONS_array;
        $scope.columnWidthOptions = editorStyleFactory.COLUMN_WIDTH_OPTIONS_array;
        $scope.colorOptions = editorStyleFactory.COLOR_OPTIONS_array;
        $scope.wordwrapOptions = editorStyleFactory.WORD_WRAP_OPTIONS_array;

        // watchers

        // updates widget's styles object
        var unbindWidgetWatcher = $scope.$watch('widget', function (val) {
            $scope.model = $$get($scope, 'widget.style');
        });


        // public methods
        // handler for click on checkbox on styles panel
        $scope.clickCheckBox = function (propertyName) {
            $scope.model[propertyName] = !$scope.model[propertyName];
            $scope.redraw();
        };

        // handler for click event on radio buttons on styles panel
        $scope.clickRadioBox = function (propertyName) {
            if (defined($scope.model[propertyName])) {

                for (var key in $scope.model) {
                    if ($scope.model.hasOwnProperty(key) && ~key.indexOf(propertyName.split('/')[0])) {
                        $scope.model[key] = false;
                    }
                }
                $scope.model[propertyName] = true;
                $scope.redraw();
            }
        };

        // used in style editor template checkboxes and radio buttons
        $scope.isSelected = function (propertyName) {
            return $scope.model[propertyName];
        };

        // redraws widget
        $scope.redraw = function () {
            _.defer(function () {
                $scope.$root.widget.redraw();
            });
        };

        // destroying widget and unbinding watchers
        var destroyUnregisterer = $scope.$on("$destroy", function () {

            unbindWidgetWatcher();
            destroyUnregisterer();
        });

    }

]);
