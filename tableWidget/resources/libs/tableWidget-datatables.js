/**
 * TableWidget implementation
 */
(function (ns) {
    "use strict";

    /**
     * TableWidget Class
     *
     * @constructor
     * @return {ns.TableWidget}
     */
    ns.NewTableWidget = function () {

        var
            /**
             * Private properties
             */
            $this = this,
            initialised = false,
            minColumnWidth = 75,
            rowHeight = 26,
            paddingHor = 8,
            paddingVer = 16,
            scrollWidth = 8,
            cachedDataLimit = 5000,
            pagingBlockHeight = 17,
            minWidthForFooter = 350,
            minFFscrollbarHeight = 34, // minimum height of block with visible vertical scrollbar in FF
            container = null, // chart container
            style = null, // widget styles
            sortedField = null, // sorted field index and direction
            cachedData = null,
            bottomBorder = null,
            hasScroll = null,
            headersHeight = null,
            isExportToPDF = $('body').attr('class')? ($('body').attr('class').indexOf('export-layout') != -1 ||
                $('#prism-window').hasClass('reporting-widget-layout')):false,
            options = {
                data: [],
                style: {},
                width: 0,
                height: 0,
                domReadyCallback: function () {
                }
            },
        /* Private methods */
        /*  Event handlers */
            scrollHandler = function (e) {
                var $this = $(this);

                $this.scrollLeft() ?
                    container.addClass('tw-scrolled-h') :
                    container.removeClass('tw-scrolled-h');

                (this.scrollLeft + this.clientWidth == this.scrollWidth) ?
                    container.addClass('tw-scrolled-h-r') :
                    container.removeClass('tw-scrolled-h-r');

                (this.scrollTop + this.clientHeight == this.scrollHeight) ?
                    container.addClass('tw-scrolled-v-b') :
                    container.removeClass('tw-scrolled-v-b');
            },
            wheelHandler = function (e) {
                e.preventDefault();
                e.stopPropagation();

                var
                    eo = e.originalEvent,
                    scrollBody = container.find('.dataTables_scrollBody'),
                // if IE - divide wrong delta
                    delta = (-eo.wheelDelta || (Math.abs(eo.deltaY) < 30 ? eo.deltaY * 40 : eo.deltaY * 1.2)) / 3
                    ;

                scrollBody.scrollTop(scrollBody.scrollTop() + delta);
                return false;
            },
            resizeHandler = function (e) {
                checkScroll();
                if (headersHeight !== container.find('.dataTables_scrollHeadInner thead').height())
                    hasScroll = true;
            },
            bottomBorderResize = function () {
                bottomBorder && bottomBorder.css('width', container.find('#grid').width() + 'px')
            },
            /**
             * Handles style changes
             */
            applyStyleChanges = function () {

                if (!container.find('.tw-table-container').length) {
                    container.empty().append('<div class="tw-table-container"></div>');
                    container = container.find('div')
                } else {
                    container = container.find('.tw-table-container');
                    container.removeAttr('class');
                    container.addClass('tw-table-container');
                }

                var classes = [];

                if (style['width/window'])     classes.push('width-window');

                if (style['wordwrap/headers']) classes.push('wordwrap-headers');
                if (style['wordwrap/rows'])    classes.push('wordwrap-rows');

                if (style['colors/headers'])   classes.push('colors-headers');
                if (style['colors/rows'])      classes.push('colors-rows');
                if (style['colors/columns'])   classes.push('colors-columns');
                if (style['scroll'])           classes.push('infinite-scroll');

                if (window.prism.$device.browser().webkit) classes.push('tw-webkit');

                classes.push(options.bordersStyle.replace('/','-'));

                container.addClass(classes.join(' '));

                firefoxHeaderCheck();
                checkScroll();
            },
            /**
             * Clears the drawing area
             */
            renderTable = function () {

                // showing loader on init
                toggleLoader(true);

                if (style['width/window']) style.tableState = {};

                var
                    tableState = style.tableState,
                    isColumnsSizes = defined(tableState, 'colResize.columns'),
                    tableSize = isColumnsSizes ? tableState.colResize.tableSize + 'px' : '100%',
                    table = $(
                        '<table id="grid" class="visibility-hidden" style="width: ' + tableSize + ';">' +
                        '<thead><tr></tr></thead>' +
                        '</table>'
                    ),
                    isHeadersEquals = tableState && tableState.headers &&
                        options.headers.length == tableState.headers.length &&
                        options.headers.every(function(item, i) {
                            return JSON.stringify(item) === JSON.stringify(tableState.headers[i]);
                        }),
                    shift = isExportToPDF ? scrollWidth : 0,
                    pagingHeight = style['scroll'] ? 0 : pagingBlockHeight,
                    displayBuffer = Math.ceil((50 * rowHeight) / (options.height)),
                    tableAPI
                    ;

                // clear cached data if metadata was changes
                if (!isHeadersEquals || options.isFiltersChanged) cachedData = [];

                // hide info block if widget width is too small
                if (options.width < minWidthForFooter) container.addClass('hide-info');

                container.css({
                    height: options.height - paddingVer,
                    width: options.width - paddingHor - shift
                });

                // showing tooltip on element when there is ellipsis
                container.on('mouseover', 'th, td', function () {
                    var $this = $(this);
                    if (this.offsetWidth < this.scrollWidth) $this.attr('title', $this.text())
                });

                // assigning width and css classes for table columns
                options.headers.forEach(function (item, i) {
                    var
                        columnWidth = (options.width - paddingVer) / options.headers.length,
                        isSorted = defined(sortedField.formula) && sortedField.formula === item.formula ||
                            (defined(sortedField.dim) && sortedField.dim === item.dim &&
                            sortedField.level === item.level && sortedField.agg === item.agg),
                        sortedClass = isSorted ? 'sorted' : '',
                        width = isColumnsSizes && tableState.colResize.columns[i] && isHeadersEquals ?
                            tableState.colResize.columns[i] : style['width/window'] ?
                            Math.max(columnWidth, minColumnWidth) + 'px' : 'auto',
                        dataString = (item.dim ? ' data-dim="' + item.dim + '" ' : '') +
                            (item.agg ? ' data-agg="' + item.agg + '" ' : '') +
                            (item.level ? ' data-level="' + item.level + '" ' : '') +
                            (item.formula ? ' data-formula="' + item.formula + '" ' : '')
                        ;

                    table.find('tr').append(
                        '<th style="width: ' + width + ';" class="' + sortedClass + '"' + dataString + '>' +
                        item.title +
                        '<div class="tw-sort-head ' + (isSorted ? sortedField.direction : '') + '"' + dataString + '>' +
                        '</th>'
                    );
                });

                container.append(table);
                headersHeight = table.height();

                firefoxHeaderCheck();

                tableAPI = table.dataTable({
                    scrollCollapse: false,
                    searching: false,
                    ordering: false,
                    sort: false,
                    paging: style['scroll'] ? undefined : true,
                    pagingType: 'full_numbers',
                    lengthChange: false,
                    pageLength: style['scroll'] ? options.queryOptions.limit : style['pageSize'],
                    info: !style['scroll'],
                    language: {
                        thousands: '',
                        info: options.pageModeInfo,
                        infoEmpty: options.noDataSign
                    },
                    stateDuration: 0,
                    scrollY: options.height - headersHeight - paddingVer - pagingHeight,
                    scrollX: style['width/content'],
                    stripeClasses: [],
                    dom: style['scroll'] ? 'JrtS' : 'Jrtpi',
                    serverSide: true,
                    autoWidth: true,
                    stateSave: style['width/content'],
                    colResize: style['width/content'] ? { resizeCallback: bottomBorderResize } : false,
                    scroller: !style['scroll'] ? undefined : {
                        loadingIndicator: false, // show indicator while scrolling
                        boundaryScale: 0.7, // 0-1 scale to redraw current view on scroll
                        displayBuffer: displayBuffer // buffer size in pages (page == number of rows in current view)
                        //serverWait: 0 // delay after scroll
                    },
                    createdRow: options.createdRowCallback,
                    responsive: style['width/content'] ? false : {
                        details: {
                            renderer: function (api, rowIdx) {
                                var data = api.cells(rowIdx, ':hidden').eq(0).map(function (cell) {
                                    var header = $(api.column(cell.column).header());
                                    var idx = api.cell(cell).index();

                                    if (header.hasClass('control') || header.hasClass('never')) {
                                        return '';
                                    }

                                    // Use a non-public DT API method to render the data for display
                                    // This needs to be updated when DT adds a suitable method for
                                    // this type of data retrieval
                                    var dtPrivate = api.settings()[0];
                                    var cellData = dtPrivate.oApi._fnGetCellData(
                                        dtPrivate, idx.row, idx.column, 'display'
                                    );

                                    return '' +
                                        '<li data-dtr-index="' + idx.column + '">' +
                                        '<span class="tw-title-wrapper">' +
                                        '<span class="dtr-title">' +
                                        header.text() +
                                        '</span> ' +
                                        '<span class="tw-colon">:</span>' +
                                        '</span>' +
                                        '<span class="dtr-data">' +
                                        cellData +
                                        '</span>' +
                                        '</li>';
                                }).toArray().join('');

                                return data ?
                                    $('<ul data-dtr-index="' + rowIdx + '"/>').append(data) :
                                    false;
                            }
                        }
                    },
                    // Makes request to the server with changed widget style object
                    stateSaveCallback: _.debounce(function (settings, currentState) {
                        tableState.start = currentState.start =
                            tableState.iScroller = currentState.iScroller =
                                tableState.iScrollerTopRow = currentState.iScrollerTopRow = 0;

                        if ( _.difference($$get(currentState, 'colResize.columns'), $$get(tableState, 'colResize.columns')).length) {

                            currentState.headers = options.headers;
                            options.widget.style.tableState = currentState;

                            if (options.canSaveChanges) options.widget.changesMade('tableState', 'style');

                            // re-render table when headers height was changed after resize
                            if (hasScroll) {
                                $this.render();
                                hasScroll = false;
                            }
                        }
                    }, 100),
                    // Returns tableState from widget.style
                    stateLoadCallback: function (settings) {
                        var addedWidth, lastColumn;

                        if (!isHeadersEquals && tableState.headers) {
                            if (options.headers.length > tableState.headers.length) {
                                tableState = {};
                                if(tableAPI) tableAPI.state.clear();
                            } else {
                                tableState.headers.forEach(function (item, i) {
                                    if (!_.some(options.headers, function (el) {return JSON.stringify(el) === JSON.stringify(item);})) {
                                        tableState.colResize.tableSize -= parseInt(tableState.colResize.columns[i], 10);
                                        tableState.colResize.columns.splice(i, 1);
                                        tableState.columns.splice(i, 1);
                                    }
                                });
                                tableState.headers = options.headers;
                            }
                        }

                        if (!style['scroll']) tableState.length = style['pageSize'];

                        tableState.start = 0;

                        return tableState;
                    },
                    ajax: function (eventData, dtCallback, settings) {
                        queryData(eventData, dtCallback);
                    }
                }).api();

                table
                    .on('init.dt', function () {
                        initialised = true;
                    })
                    .on('processing.dt', function (e, settings, processing) {

                        var info = container.find('#grid_info');
                        // handling table footer behavior
                        if ((info.width() + container.find('#grid_paginate').width()) > options.width - minColumnWidth) {
                            info.hide(0);
                        } else {
                            info.show(0);
                        }

                        if (initialised) {
                            initialised = false;

                            container.find('table').removeClass('visibility-hidden');

                            // fixing table height after columns resize
                            $(document).off('mouseup.ColResize');
                            $(document).on('mouseup.ColResize', resizeHandler);

                            container.find('.dataTables_scrollHead').off('mousewheel', wheelHandler);
                            container.find('.dataTables_scrollHead').on('mousewheel', wheelHandler);

                            container.find('.dataTables_scrollBody').off('scroll', scrollHandler);
                            container.find('.dataTables_scrollBody').on('scroll', scrollHandler);

                            var
                                tables = container.find('.dataTables_wrapper'),
                                $table = container.find('.dataTables_wrapper:eq(0) #grid')
                                ;

                            //removing previous DataTable instance
                            if (tables.length > 1) {

                                if (style['width/window'] && $.fn.DataTable.isDataTable($table)) {

                                    var tableAPI = $table.DataTable();

                                    tableAPI.state.clear();
                                    tableAPI.destroy();
                                }

                                tables.eq(0).remove();
                            }

                            // adding sorting icons to headers
                            initSorting();

                            // hiding loader on render end
                            toggleLoader();

                            checkScroll();
                            helperElements();
                            // triggering domReady event for PDF export
                            options.domReadyCallback();
                        }

                        bottomBorderResize();

                    });
            },

            queryData = function(eventData, dtCallback) {
                eventData.start = parseInt(eventData.start, 10);
                eventData.length = parseInt(eventData.length, 10);

                var
                    sum = eventData.start + eventData.length,
                    currentData
                    ;

                if (eventData.start) {

                    if (defined(cachedData[eventData.start]) && defined(cachedData[sum])) {

                        dtCallback({
                            draw: eventData.draw,
                            data: cachedData.slice(eventData.start, sum),
                            recordsTotal: cachedData.length,
                            recordsFiltered: cachedData.length
                        });

                    } else {
                        toggleLoader(true);

                        options.dataService.getData(
                            options.widget,
                            eventData.start,
                            options.queryOptions.limit,
                            options.queryOptions.ungroup,
                            cb
                        );
                    }

                } else {
                    // initial load
                    setTimeout(function () {
                        dtCallback({
                            draw: eventData.draw,
                            data: formatData(options.data, eventData.start, eventData.length),
                            recordsTotal: cachedData.length,
                            recordsFiltered: cachedData.length
                        });
                    });
                }

                function cb (response) {
                    var
                        rows = response,
                        rowsLength = $$get(rows, 'length') || 0
                        ;

                    if (rowsLength) currentData = formatData(rows, eventData.start, eventData.length);

                    if (currentData) {
                        dtCallback({
                            draw: eventData.draw,
                            data: currentData,
                            recordsTotal: cachedData.length,
                            recordsFiltered: cachedData.length
                        });
                    }
                    toggleLoader();
                }
            },

            formatData = function (data, offset, limit) {
                var
                    formatted = [],
                    len = Math.max(data.length, limit)
                    ;

                for (var i = 0; i < len; i++) {
                    var row = [];

                    if (!data[i]) continue;
                    for (var j = 0; j < data[i].length; j++) {
                        if (options.headers[j].datatype === 'numeric' && options.numberFormatters[j]) {
                            row.push(options.numberFormatters[j](data[i][j].data));
                        } else if (offset && options.headers[j].datatype === 'datetime' && options.datetimeFormatters[j]) {
                            row.push(options.datetimeFormatters[j](data[i][j].data));
                        } else {
                            row.push(data[i][j].text);
                        }

                        // used for debugging
                        //if (j === 0) {
                        //    row[j] = (offset + i + 1)  +  ' . '  + row[j];
                        //}
                    }
                    formatted.push(row);
                    cachedData[offset + i] = row;
                }
                return style['scroll'] ? formatted : formatted.slice(0, style['pageSize']);
            },

            initSorting = function () {

                container.find('table thead th').on('click', '.tw-sort-head', function (e) {
                    var
                        $this = $(this),
                        dim = $this.data('dim'),
                        agg = $this.data('agg'),
                        level = $this.data('level'),
                        formula = $this.data('formula'),
                        classList = $this.attr('class').split(' '),
                        isSorted = _.some(classList, function (item) {
                            return ~['asc', 'desc'].indexOf(item);
                        }),
                        isAsc = ~classList.indexOf('asc'),
                        direction = !isSorted || isSorted && !isAsc ? 'asc' : 'desc'
                        ;

                    options.sortingService.sortByColumn(dim, level, agg, formula, direction, options.widget, options.canSaveChanges);
                });
            },

            helperElements = function () {
                if (!container.find('dataTables_scroll .tw-bottom-border').length) {
                    // minus 1px for proper gradient rendering in PDF/Image export
                    container.find('.dataTables_scroll').append(
                        '<div class="tw-bottom-border" style="width: ' +
                        (style['width/content'] ?
                            $$get(style, 'tableState.colResize.tableSize') :
                        container.find('#grid').width() - (isExportToPDF ? 1 : 0)) +
                        'px"></div>'
                    );

                    bottomBorder = container.find('.tw-bottom-border');
                }
                // minus 2px for proper gradient rendering in PDF/Image export
                var height = container.find('.dataTables_scrollHead').height() +
                    container.find('#grid').height() - (isExportToPDF ? 2 : 0);

                if (!container.find('dataTables_scroll .tw-left-border').length) {
                    container.find('.dataTables_scroll').append(
                        '<div class="tw-left-border" style="height: ' + height + 'px"></div>'
                    );
                }

                if (!container.find('dataTables_scroll .tw-right-border').length) {
                    container.find('.dataTables_scroll').append(
                        '<div class="tw-right-border" style="height: ' + height + 'px"></div>'
                    );
                }
            },

            handleNoData = function () {
                container.empty().attr('class', '');
                // triggering domReady event for PDF export
                options.domReadyCallback();
            },

        // workaround for Firefox (PRIS-8836)
            firefoxHeaderCheck = function () {
                if (window.prism.$device.browser().ff && (headersHeight < minFFscrollbarHeight))
                    container.addClass('tw-ff');
                else
                    container.removeClass('tw-ff');
            },

            toggleLoader = function (on) {
                on ? container.addClass('DTS_Loading') : container.removeClass('DTS_Loading');
            },

            checkScroll = function () {
                var scrollBody = container.find('.dataTables_scrollBody');

                if (scrollBody.length) {
                    // adding gradient in table with scroll in PDF/Image export
                    // and borders for table with scroll
                    if (scrollBody[0].offsetHeight < scrollBody[0].scrollHeight) {
                        container.addClass('tw-vertical-scroll');
                    } else {
                        container.removeClass('tw-vertical-scroll');
                    }

                    if (scrollBody[0].offsetWidth < scrollBody[0].scrollWidth + scrollbarWidth) {
                        container.addClass('tw-horizontal-scroll');
                    } else {
                        container.removeClass('tw-horizontal-scroll');
                    }
                }
            };

        /* Public methods */
        /**
         * Renders treemap on the page with the given options
         *
         * @param {Object} opts
         * @return {ns.TableWidget}
         */
        this.render = function (opts) {

            options = $.extend(options, opts);
            style = options.style;
            cachedData = options.cachedData;
            container = options.container;

            if (cachedData.length > cachedDataLimit) {
                cachedData = cachedData.slice(cachedData.length);
            }

            applyStyleChanges();

            if (options.isStyleChanged) return this;

            sortedField = options.sortingService.getSortedField(options.widget);

            options.width = container.parent().parent().width();
            options.height = container.parent().parent().height();

            if (!container || !container.length) {
                throw new TypeError('TableWidget render error - DOM container element is missing!');
            }

            if (options.isData) {
                renderTable();
            } else {
                handleNoData();
            }

            return this;
        };
    };

    ns.NewTableWidget.prototype.constructor = ns.TableWidget;

}(window));

if ($.fn.dataTableExt) $.fn.dataTableExt.sErrMode = 'throw';
