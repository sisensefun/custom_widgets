/**
 * Service for sorting data in Table widget
 *
 * @author Volodymyr Sheremeta <volodymyr.sh@sisense.com>
 */
mod.service('sortingService', [

    function(){

        this.sortByColumn = function (dimension, level, agg, formula, direction, widget, canSaveChanges) {
            var panelItems = widget.metadata.panel('columns').items;

            panelItems.forEach(function (item, i) {

                if (item.disabled) return;

                delete item.jaql.sort;
                delete item.jaql.sortDetails;

                if (defined(formula) && item.jaql.formula === formula ||
                    defined(dimension) && item.jaql.dim === dimension && item.jaql.level === level && item.jaql.agg === agg) {

                    item.jaql.sort = direction;
                }
            });

            _.defer(function () {
                widget.refresh();

                if (canSaveChanges)
                    widget.changesMade('sort', 'metadata');
            });
        };

        this.getSortedField = function (widget) {
            var
                panelItems = widget.metadata.panel('columns').items,
                o = null
                ;

            panelItems.forEach(function (item, i) {
                if (!o && !item.disabled && item.jaql.sort) {
                    o = {
                        dim: item.jaql.dim,
                        agg: item.jaql.agg,
                        level: item.jaql.level,
                        direction: item.jaql.sort,
                        formula: item.jaql.formula
                    };
                }
            });
            return o || {};
        };
    }
]);