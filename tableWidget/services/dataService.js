/**
 * Service for querying data for infinite scroll in Table widget
 *
 * @author Volodymyr Sheremeta <volodymyr.sh@sisense.com>
 */
mod.service('dataService', [
    'dashboard-base.services.$query',

    function($query){

        this.getData = function (widget, offset, limit, ungroup, callback) {

            var query = $query.buildWidgetQuery(widget);

            //query.ungroup = ungroup;
            query.count = parseInt(limit, 10);
            query.offset = parseInt(offset, 10);

            $query.jaql(query).then(function (response) {
                //console.info('response: offset, length, obj', response[0].config.data.offset, response[0].data.values.length, response);
                callback(defined(response, '0.data.values') ? response[0].data.values : [], null);
            }).catch(function (response) {
                callback(null, response);
                $$.error(response);
            });
        };
    }
]);