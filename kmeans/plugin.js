// this is a plugin

prism.run([
    'widget-editor.services.$popper',
	'ux-controls.services.$dom',
	function($popper, $dom){
				
		prism.on("beforemenu", function(){
			
			if(arguments[1].ui.css === "w-menu-host" && arguments[0].currentScope.widget.type == "chart/scatter"){
				
				var goodScope = arguments[0].currentScope;
				
				var menuItem = {
					command: {
						title: "k-means",
						desc: "K-means clustering",
						canExecute: function(){ return true;},
						execute: function(){ popKmeansWindow(goodScope); }
					}
				}
				
				arguments[1].settings.items.push({
					caption: "Clustering",
					type: 'header'
				});
				arguments[1].settings.items.push(menuItem);
				
			}
		});
		
		function popKmeansWindow(goodScope){
		
			$dom.modal({
				scope: {
					goodScope: goodScope,
					onCloseFn: function(){
						$('.md-overlay').remove();
						$('.md-modal.md-effect.md-show').remove();
					}
				},
				templateUrl: '/plugins/kmeans/views/pop.html'
			});
		}	
	}
]);

mod.controller('viewDetails',[
	'$scope',
	'$http',
	'widget-editor.services.$popper',
	'ux-controls.services.$dom',
	'base.factories.colorFactory','base.services.$naming',
	'ux-controls.services.$popper',
	function($scope, $http,$popper,$dom,colorFactory,$naming,$uxpop){
	
	$scope.config = mod.config;
	
	$scope.numClusters = $scope.config.minClusters;
	
	$scope.measures = [];
	
	$scope.popjaql = function (ev, goodScope){
		
		var closeFunction =
		$popper.popJaql(
			goodScope, //selectionSession.scope, // scope
			{dimensions: false, measures: true}, // ual types
			null, // jaql to edit
			ev, // event
			null, // pre-cancel callback
			null, // post-cancel callback
			function (jaql, closeEvent) {
				if($scope.measures.length<$scope.config.maxMeasures){
					jaql.title = $naming.getTitle(jaql).plain;
					$scope.measures.push(jaql);
				}
				
				closeFunction();
			}, // success callback
			'Select clustering measure'  // title key (translation)
			// no navigation ?
			// on close callback
			);
	
	};
	
	$scope.save = function(ev){
		
		if(prism.activeWidget.metadata.panels[3].items.length > 0)
			$uxpop.popConfirmation("\"Color by\" metadata is already defined, and will be replaced. Are you sure?", ev, doSave, function(){});
		else
			doSave();
	};
	
	function doSave(){
		// create jaql & format for "color by" item
		var jaql = formulaCreator($scope.measures, $scope.numClusters)
			format = formatCreator($scope.numClusters);
			
		// add item
		prism.activeWidget.metadata.panels[3].items.splice(0, 1, { jaql : jaql, format : format});
		
		// refresh widget
		prism.activeWidget.refresh();
		
		// close window
		$scope.in.onCloseFn();
	}
	
	$scope.removeMeasure = function(idx){
		
		$scope.measures.splice(idx, 1);
	}
	
	function formulaCreator(jaqls, clusters){
				
		// convert list of jaqls to tokens
		var cols = jaqls.length, context = _.object(_.map(jaqls, function(item, idx){
			return ["[token"+idx+"]", item];
		}));
		
		// create formula string
		var formula = 'RINT(TRUE, "m<-log(matrix(unlist(args), ncol = ' + cols + ')); kmeans(m,' + clusters + ')$cluster", ' + Object.keys(context).join(', ') + ')';
		
		// create valid JAQL
		var jaqlObj = {
			formula: formula,
			context: context,
			title: "kmeans",
			type: "measure"
		};
		
		return jaqlObj;
	}
	
	function formatCreator(clusters){

		var conditions = [];		
		
		for(var i=0;i<clusters;i++){
		
			conditions.push({
				color: colorFactory.defaultFormat.palette[i][2],
				operator: "=",
				expression: i+1
			});
		
		}
		
		var colorFormat = {
		  "type": "condition",
		  "conditions": conditions
		};
		
		return { color: colorFormat };
	}
		
}]);


