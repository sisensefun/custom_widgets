prism.run([function() {
	// Registering dashboard/ widget creation in order to perform drilling
	prism.on("dashboardloaded", function (e, args) {
		args.dashboard.on("widgetinitialized", onWidgetInitialized);
	});

	// register widget events upon initialization
	function onWidgetInitialized(dashboard, args) {
		// hooking to pivot ready/destroyed events
		if (args.widget.type === "pivot" && defined(args, "widget.options.imageColumn")) {
			//	For pivot widgets			
			args.widget.on("processresult", addImagePivot);
		} else if (args.widget.type === "tablewidget" && defined(args, "widget.options.imageColumn")) {
			// 	For table widgets
			var tableElement = $('.tablewidget-widget-content', $('widget[widgetid=' + args.widget.oid + ']'));
			tableElement.on("draw.dt", updateImageTable);
		}
		args.widget.on("destroyed", onWidgetWasDestroyed);
	}

	// unregistering widget events
	function onWidgetWasDestroyed(widget, args) {
		if (widget.type === "pivot") {
			//	For pivot widget
			widget.off("processresult", addImagePivot);
		} else {
			//	For table widget
			widget.off("draw.dt", updateImageTable);
		}		
		widget.off("destroyed", onWidgetWasDestroyed);
	}

	// create dashboards menu
	prism.on("beforemenu", function (e, args) {
		try {
			
			//	Define menu caption
			var menuCaption = "Image URL Column";

			//	Hooking to the WidgetEditor Menu
			if (!$(args.ui.target).hasClass('wet-menu')) {
				return;
			}

			//	Has the menu been added already?
			var addMenuItem = true;
			$.each(args.settings.items, function(){
				if (this.caption == menuCaption) {
					addMenuItem = false;
				}
			})

			if (addMenuItem) {
				//	Save column selection
				function setColumn() {
					//	Set the image column as an option
					prism.activeWidget.options.imageColumn = {fidx : this.fidx, caption: this.caption};
					//	Update the widget now
					//addImages(prism.activeWidget,null);
				}

				//	Look for existing setting
				var myColumn = prism.activeWidget.options.imageColumn;

				//	Create array of column options
				var columns = [];

				//	Create "disabled" option
				var disabledMenuItem = {
					caption: "Disable",
					type: "option",
					execute: setColumn,
					checked: (!defined(myColumn) ||  myColumn.fidx < 0) ? true: false,
					fidx: -1
				};
				columns.push(disabledMenuItem);

				//	Get the available dimensions
				if (e.currentScope.widgetType == "tablewidget"){

					//	Working with a table widget
					var dimensions = $.grep(prism.activeWidget.metadata.panels, function(a) { return a.name == "columns"; })[0];

					//	Loop through the list, and create sub-menus
					$.each(dimensions.items, function(i) {
						var index = i+1;
						var menuItem = {
							caption: this.jaql.column,
							type: "option",
							execute: setColumn,
							checked: (defined(myColumn) &&  index == myColumn.fidx) ? true: false,
							fidx: index
						};
						columns.push(menuItem);
					})

				} else {

					//	Working with a pivot widget
					var dimensions = $.grep(prism.activeWidget.metadata.panels, function(a) { return a.name == "rows"; })[0];

					//	Loop through the list, and create sub-menus
					$.each(dimensions.items, function() {
						var menuItem = {
							caption: this.jaql.column,
							type: "option",
							execute: setColumn,
							checked: (defined(myColumn) &&  this.field.index == myColumn.fidx) ? true: false,
							fidx: this.field.index
						};
						columns.push(menuItem);
					})
				}

				//	Add the new menu items
				args.settings.items.push({type: "separator"});
				args.settings.items.push({
					caption: menuCaption,
					items: columns
				});
			}

		}
		catch (e) {
			console.log(e);
		}
	});

	//create dashboard links with filters for each pivot cell
	function addImagePivot(widget, query, result, rawResult, reason) {
		try {
			//	Get the column index
			var index = widget.options.imageColumn.fidx;
			//	Get the HTML table
			var table = $(query.result.table);
			//	Get the selected cells
			var cells = $('td[fidx='+index+'][class*=p-dim-member]',table)
			//	Loop through each cell and update the contents
			$.each(cells, function() {
				//	Create the HTML
				if ($(this).text().length > 0) {
					var imgTag = "<div style='margin:0 auto;'><img src='" + $(this).text() + "'></img></div>";
					//	Override the existing cell contents
					$(this).html(imgTag);
					//	Add CSS styling to center the images
					$(this).css("text-align","center");
				}
			})	
			//	Write the new HTML table back to the widget
			query.result.table = table[0].outerHTML;						
		}	
		catch (e){
			console.log(e);
		}
	}

	function updateImageTable(event, settings) {		
		try {
			//	Get the current widget's html element
			var element = this.parentElement.parentElement;
			//	Get the widget id
			var widgetId = $(element).attr("widgetid");
			//	Get the widget object
			var widget = $.grep( prism.activeDashboard.widgets.$$widgets, function(a) { 
				return a.oid == widgetId; 
			})[0];
			//	Get the column index
			var fidx = widget.options.imageColumn.fidx;
			//	Make sure some column is specified
			if (fidx >= 0) {
				//	Get the current widget's element
				var element = $('widget[widgetid=' + widget.oid + ']');
				if (element.length == 0)
				{
					element = $('.widget-body');
				}
				//	Get the table cells to modify
				var cells = $('tr td:nth-child(' + fidx + ')',element);
				//	Loop through each cell and update the contents
				$.each(cells, function() {
					//	Does the image need to be added?
					var isNew = $(this).html().indexOf("<img") == -1;
					if (isNew) {
						//	Create the HTML
						var imgTag = "<div style='margin:0 auto;'><img src='" + $(this).text() + "'></img></div>";
						//	Override the existing cell contents
						$(this).html(imgTag);
						//	Add CSS styling to center the images
						$(this).css("text-align","center");
					}
				})	
			}		
		}
		catch (e) {
			console.log(e);
		}
	}
}]);