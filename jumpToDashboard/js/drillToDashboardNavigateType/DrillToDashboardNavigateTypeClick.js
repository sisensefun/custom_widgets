// Drill To Dashboard Navigate Type Click Class



function DrillToDashboardNavigateTypeClick(widget){
    this.allowedTypes = ["indicator", "imageWidget", "richtexteditor"];
    DrillToDashboardNavigateType.call(this, widget);

    //set widget events
    if (widget.$$events['ready'].handlers.indexOf(this.create) < 0 ) {
        widget.on('ready', this.create);
        widget.on("destroyed", removeOnReady);
    }

    // unregistering widget events
    function removeOnReady(widget, args) {
        widget.off("ready",  this.create);
        widget.off("destroyed", removeOnReady);
    }
}

DrillToDashboardNavigateTypeClick.prototype = Object.create(DrillToDashboardNavigateType.prototype);
DrillToDashboardNavigateTypeClick.prototype.constructor = DrillToDashboardNavigateTypeClick;

//create drill to dashboard navigation by click
DrillToDashboardNavigateTypeClick.prototype.create = function(widget, args) {
    if (widget.options.drillTarget) {
        var element = getWidgetElement(widget);

        var clickHandler = function (){
            var filters = getFilters(widget);
            widget.drilledDashboardDisplay.display(filters, widget.drillToDashboardConfig, widget.options.drillTarget);
        };

        //on click get filters and display drilled dashboard
        if (!widget.drillToDashboardConfig.dirty) {
            $(element).unbind("click",clickHandler);
            $(element).on("click", clickHandler);
            $._data($(element)[0]).handlerFunc = clickHandler;
            $(element).addClass("clickable");
            widget.drillToDashboardConfig.dirty = true
        }
    }
};
