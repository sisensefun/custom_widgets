
//returns method for right click menu creation depending on widget type
var RightClickMenuFactory = function(){

    //create drill to dashboard navigation by right click on an custom widget
    function createCustomMenuItem(widget){
        var createItem = function(widget) {
            var selector;

            switch (widget.type) {

                case 'imageWidget':
                    selector = '.image-container img';
                    break;

                case 'indicator':
                    selector = 'indicator-number';
                    break;
            }

            //get clickable indicator element
            var element = getWidgetElement(widget);

            if(selector) {
                element = element.find(selector);
            }

            //add menu item on right click
            $(element).on("contextmenu", function (event){

                //remove default right click behavior
                event.preventDefault();

                //get menu service
                var domService = widget.$dom;
                var items = [];

                //get filters
                var filters = getFilters(widget, element);

                addMenuItem(items, widget, filters);
                domService.menu(
                    {
                        items: items,
                        ok: function (mi) {},
                        onClosed: function () {
                            //added double closing for data-menu, sometimes not closing from $dom service
                            $('data-menu').remove();
                            $('.popup-overlay').remove();
                        }
                    },
                    {target: element, place: 'r', anchor: 't', x: event.clientX, y: event.clientY, css: "drillToDashboardMenu"});

            });
        };

        //set widget events
        if (widget.$$events && widget.$$events['ready'].handlers.indexOf(createItem) < 0 ) {
            widget.on('ready', createItem);
            widget.on("destroyed", removeOnReady);
        }

        // unregistering widget events
        function removeOnReady() {
            widget.off("ready",  createItem);
            widget.off("destroyed", removeOnReady);
        }
    }

    //create drill to dashboard navigation by right click on a pivot dimension cells
    function createPivotMenuItem(ev, args){
        if (args.settings.name != "datapoint") {
            return;
        }

        var filters = getFilters(args.settings.widget);
        var pivotFilters = getCellFilters(args.settings.widget, args.ui.target);

        filters = mergeFilters(pivotFilters, filters);

        addMenuItem(args.settings.items, args.settings.widget, filters);
    }

    //create drill to dashboard navigation by right click
    function createMenuItem(ev, args) {
        if (args.settings.name != "datapoint") {
            return;
        }

        var widget = args.settings.widget;
        if (!widget.options.drillTarget && !widget.drillToDashboardConfig.dashboardIds.length){
            return;
        }

        var selectionFilters = args.settings.jaql || [];
        var filters = getFilters(args.settings.widget, args.ui.target);
        filters = mergeFilters(selectionFilters, filters);
        addMenuItem(args.settings.items, args.settings.widget, filters);
    }

    //get menu creation function depending on widget type
    this.getMenuCreationFunction = function (widget) {
        var func;
        switch(widget.type){
            case "indicator":
            case "imageWidget":
            case "richtexteditor":
                //set widget events
                func = createCustomMenuItem;
                break;
            case "pivot":
                func = createPivotMenuItem;
                break;
            default:
                func = createMenuItem;
                break;
        }

        return func;
    };


    // create menu item w/w.o. subitems
    var getMenuItem = function(widget, drillConfig, caption, subItems, filters){
        return subItems.length ?
        {
            caption: caption,
            items: subItems
        }
            :
        {
            caption: caption,
            closing: true,
            execute: function () {
                widget.drilledDashboardDisplay.display(filters, drillConfig, widget.options.drillTarget);
            }
        };
    };

    // create menu item subitems if multiple target dashboards is set up
    var getMenuSubItems = function(widget, drillConfig, filters){
        var subItems = [];
        if (drillConfig.dashboardIds.length){ // Has multiple dashboard to drill to. Set up in widget script
            drillConfig.dashboardIds.forEach(function(item){
                subItems.push({
                    caption: item.caption,
                    closing: true,
                    execute: function () {
                        widget.drilledDashboardDisplay.display(filters, drillConfig, {oid: item.id});
                    }
                })
            });
        }

        return subItems;
    };

    //add menu item to right click menu
    var addMenuItem = function(items, widget, filters){
        var drillConfig = widget.drillToDashboardConfig;
        var caption = drillConfig.drillToDashboardRightMenuCaption;
        var subItems = getMenuSubItems(widget, drillConfig, filters);

        if (defined(widget.options, "drillTarget.caption")){
            caption += widget.options.drillTarget.caption;
        }

        if (_.find(items, function (item) {return item.caption == caption})) {
            return;
        }

        items.push({type: "separator"});

        var menuItem = getMenuItem(widget, drillConfig, caption, subItems, filters);

        items.push(menuItem);
    };
};
