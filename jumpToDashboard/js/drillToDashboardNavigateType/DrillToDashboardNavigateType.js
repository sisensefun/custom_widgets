// Drill To Dashboard Navigate Type Class

function DrillToDashboardNavigateType(widget) {
    //check if widget type supported
    if (this.allowedTypes.indexOf(widget.type) == -1){
        return;
    }

    //set drilled dashboard display mode
    switch(widget.drillToDashboardConfig.drilledDashboardDisplayType) {
        case 1:
            widget.drilledDashboardDisplay = new DrilledDashboardDisplayNewTab();
            break;
        case 2:
            widget.drilledDashboardDisplay = new DrilledDashboardDisplayModal();
            break;
        case 3:
            widget.drilledDashboardDisplay = new DrilledDashboardDisplayCurrentTab();
            break;
        default:
            widget.drilledDashboardDisplay = new DrilledDashboardDisplayNewTab();
            break;
    }
}

DrillToDashboardNavigateType.prototype.allowedTypes = [];

//create drill to dashboard navigation
DrillToDashboardNavigateType.prototype.create = function(){};

//get all filters
function getFilters(widget, target) {
    var widgetFilters = getWidgetFilters(widget, target);
    var dashboardFilters = widget.dashboard.filters.getEnabledItems();

    var filters = getDashboardOrWidgetFilters(widgetFilters).concat(getDashboardOrWidgetFilters(dashboardFilters));

    return getFiltersWithoutDuplicates(filters);
}

// get widget filters together with measured filters (those which are set up in formula)
function getWidgetFilters(widget, target){
    var measuredFilters = [];
    var widgetFilters = widget.metadata.filters();
    var valuesPanel = widget.metadata.panel("values");
    var seriesName = $$get(target, "point.series.name");

    if(valuesPanel && seriesName){
        var panelItem = valuesPanel.items.filter(function(item){
            var title = $$get(item, "jaql.title");
            return title && item.jaql.title === seriesName;
        })[0];
        if(panelItem){
            var context = panelItem.jaql.context;
            if(context){
                for(var contextItemJaql in context){

                    if(context.hasOwnProperty(contextItemJaql) && context[contextItemJaql].filter){
                        measuredFilters.push(({jaql: context[contextItemJaql]}));
                    }
                }

            }
        }
    }

    return measuredFilters.concat(widgetFilters);
}

function getDashboardOrWidgetFilters(dashboardOrWidgetFilters){
    var filters = [];

    dashboardOrWidgetFilters.forEach(function (f) {
        if(f.isCascading){
            filters.push({levels: _.clone(f.levels), model:_.clone(f.model), isCascading: true});
        }else{
            filters.push({
                jaql:  _.clone(f.jaql)
            });
        }
    });

    return filters;
}

// iterate through filters and remove those containing duplicate dimensions or remove cascading filter levels which contain duplicate dimension
function getFiltersWithoutDuplicates(filters){
    if(!filters.length){
        return filters;
    }
    var result = [];

    filters.forEach(function(filterItem, idx){
        result.push(filterItem);

        if(!filterItem.isCascading && filterItem.jaql.dim){
            var dim = filterItem.jaql.dim.toLowerCase();
            var remainingFilters = filters.slice(idx + 1);
            if(!remainingFilters.length) return;

            var duplicates = findDuplicateFilters(dim, remainingFilters);

            if(duplicates.length){
                duplicates.forEach(function(f){
                    if(f.isCascading){
                        removeLevels(f, dim);
                    }else{

                        var index = filters.indexOf(f);
                        if (index < 0) {
                            return;
                        }
                        filters.splice(index, 1);

                    }
                })
            }
        }
    });
    return result;
}

function removeFilter(by) {

    var index = -1;
    var findStr = by;

    if (_.isObject(by)){
        findStr = by.isCascading ? by.getDimensions()[0] : by.jaql.dim;
    }

    // dimensionality input
    if (_.isString(by) || _.isObject(by)) {

        for (var i = 0; i < this.$$items.length; i++) {

            var currItem = this.$$items[i];

            if ((defined(currItem.hasDimension) && currItem.hasDimension(findStr)) ||
                (!defined(currItem.hasDimension) && currItem.jaql.dim == findStr)) {

                index = i;
                break;
            }
        }
    }

    // index input
    else if (_.isNumber(by)) {

        index = by;
    }

    // verifying index
    if (index < 0 || index > this.$$items.length) {

        throw 'index out of range';
    }
    var modifiedItem = this.$$items[index];

    var unUnionPerformed = (options.unUnionIfSameDimensionAndSameType) ? modifiedItem.unmergeWith(by)  /*this.tryUnUnion(this.$$items[i], by) */: false;

    if (!unUnionPerformed) {
        this.$$items.splice(index, 1);
    } else {

        // When unselecting in a cascading filter, need to start a refresh cycle.
        if(defined(modifiedItem.refresh) && modifiedItem.isCascading)
            modifiedItem.refresh(by.jaql || by);

        this.$$handleEmptyMembersFilterIfNecessary(modifiedItem);
    }

};

// remove cascading filter levels for specific dimension
function removeLevels (filter, dim){
    var levels = filter.levels;
    for (var i = levels.length - 1; i >= 0; i--) {
        if(levels[i].dim.toLowerCase() === dim){
            levels.splice(i, 1);
        }
    }
}

// find filters by dimension
function findDuplicateFilters(dim, filters){
    return filters.filter(function(item){
        if(item.isCascading) { // for cascading filter find those which contain levels for specific dimension
            var filters = item.levels.filter(function (item) {
                return item.dim.toLowerCase() === dim;
            });
            return !!filters.length;
        }

        return !item.isCascading && item.jaql.dim && item.jaql.dim.toLowerCase() === dim;
    });
}

//merge filters excluding those with duplicate dimensions
function mergeFilters(firstFilters, secondFilters){
    return getFiltersWithoutDuplicates($.merge(firstFilters, secondFilters));
}

//get cell filters
function getCellFilters(widget, cell){
    //create filter object
    function createFilter(items, fieldIndex, value){
        var item = {jaql:{}};
        var tmpitem = $.grep(items,function(i){return i.field.index == fieldIndex})[0];

        item.jaql.datatype = tmpitem.jaql.datatype;
        item.jaql.dim = tmpitem.jaql.dim;
        item.jaql.filter = value ? {members : [value]} : {all:true};
        item.jaql.title = tmpitem.jaql.title;

        if (tmpitem.jaql.level) {
            item.jaql.level = tmpitem.jaql.level;
        }

        return item;
    }

    function isLowermostMeasureHeader(classString){
        return (classString.indexOf("p-measure-head") >=0 ||
        classString.indexOf("p-total-head") >= 0 &&
        classString.indexOf("p-dim-member") < 0 ||
        classString.indexOf("p-grand-total-head") >= 0);
    }

    function isLowermostHeaderClass(classString){
        return (classString.indexOf("p-dim-head") >= 0 || isLowermostMeasureHeader(classString));
    }

    function getLowermostHeaderCellsInOrder(table){
        function scanSubtree(rowIndex){
            for(;leavesToFindInCurrentSubtree[rowIndex] > 0;)
                scanSubtree(rowIndex + rowSpansOfCurrentSubtree[rowIndex]);

            var indexOfCellWithinRow = cellIndices[rowIndex];
            var currentRow = headerRows[rowIndex];

            if(currentRow && !(currentRow.cells.length <= indexOfCellWithinRow)){
                var numberOfLeavesInSubtree = parseInt(currentRow.cells[indexOfCellWithinRow].getAttribute("colspan") || 1);
                var rowSpan = parseInt(currentRow.cells[indexOfCellWithinRow].getAttribute("rowspan") || 1);

                if((numberOfLeavesInSubtree > 1 || !isLowermostHeaderClass(currentRow.cells[indexOfCellWithinRow].className)) && headerRows.length >= rowIndex + rowSpan + 1)
                    return leavesToFindInCurrentSubtree[rowIndex] = numberOfLeavesInSubtree || 1, cellIndices[rowIndex]++, rowSpansOfCurrentSubtree[rowIndex] = rowSpan, void scanSubtree(rowIndex+rowSpan);

                elements.push(currentRow.cells[indexOfCellWithinRow]), cellIndices[rowIndex]++;

                for(var i=0;rowIndex>i;++i)leavesToFindInCurrentSubtree[i]--;scanSubtree(0)}
        }

        for(var elements=[],headerRows=$(table).find("thead tr:not(.p-fake-measure-head)"),cellIndices=[],leavesToFindInCurrentSubtree=[],rowSpansOfCurrentSubtree=[],i=0;i<headerRows.length;++i)
            cellIndices.push(0),leavesToFindInCurrentSubtree.push(0),rowSpansOfCurrentSubtree.push(0);

        return scanSubtree(0),elements
    }

    // given a cell, gets the dimension member row cells for that cell, for all row dimensions. (i.e, for a value cell of row 'tel aviv -> israel -> asia', returns the cells for 'tel aviv', 'israel', 'asia')
    // cell can be either a value cell, or a dimension member cell
    function getRowCells(cell, dimensionsContainer) {
        var dimCellId = $(cell.parentNode.cells).filter('.p-dim-member:last, .p-total-row-head:last, .p-grand-total-row-head:last').prop('id'); //last dimension is guaranteed to be in the same row as value
        var currCell = cell.className.indexOf('p-dim-member') >= 0 ? cell : dimensionsContainer.find('td[originalid = "' + dimCellId + '"], td#' + dimCellId)[0];	// get corresponding cell in the dimensions container
        var allDimensionMemberCells = $(currCell).parents('tbody').find('td.p-dim-member');
        var rowCells = [];
        var lastFoundCell = currCell;
        var currCellIndex = parseInt(currCell.getAttribute('fIdx'));

        if (currCellIndex == 0 || $(currCell).hasClass("p-grand-total-row-head")) {
            return [currCell];
        }

        while (currCellIndex > 0) {
            var currRow = lastFoundCell.parentNode;

            while (!currCell) {
                currRow = node_before(currRow);
                currCell = $(currRow).find('td.p-dim-member[fIdx = ' + (currCellIndex - 1) + ']')[0];
            }

            while (currCell) {
                currCellIndex = parseInt(currCell.getAttribute('fIdx'));
                rowCells = $.merge(rowCells, [currCell]);
                lastFoundCell = currCell;
                currCell = node_before(currCell);
            }
        }

        return rowCells;
    }

    function node_before(sib){
        while(sib = sib.previousSibling) {
            if (!is_ignorable(sib)) {
                return sib;
            }
        }
        return null;
    }

    function is_ignorable(nod){
        return 8==nod.nodeType || 3==nod.nodeType && is_all_ws(nod);
    }

    function is_all_ws(nod){
        return !/[^\t\n\r ]/.test(nod.data);
    }

    function getCellsByIndexAndValue(allCells,fieldIndex,value,isDateTime){
        function exactTextMatch(){
            return isDateTime?_.isEqual(Date.parseDate(this.getAttribute("val")),value):this.getAttribute("val") === value;
        }

        var matchingCells = allCells.filter(exactTextMatch).filter("[fIdx = "+fieldIndex+"], [fDimensionIdx = "+fieldIndex+"]");

        return matchingCells;
    }

    function getCellsFromSameFieldWithSameTextValue(allCells,cell){
        var value = $(cell).attr("val"),fieldIndex=parseInt(cell.getAttribute("fIdx"));

        return getCellsByIndexAndValue(allCells,fieldIndex,value);
    }

    // given a cell, gets the dimension member column header cells for that cell, for all column dimensions. (i.e for a value cell of column 'ipod nano -> apple' returns 'ipod nano', 'apple'
    // cell can be either a value cell, or a dimension member header cell.
    function getColumnCells(cell, headersContainer, orderedLowermostCells, isLowermostHeaderCell) {
        var columnCells = [];
        var headerCell = cell;
        if (!isLowermostHeaderCell) {
            var indexAmongstValueCells = $(cell).parent().children('td.p-value').index(cell);
            headerCell = _.reject(orderedLowermostCells, function(c){
                return c.className.indexOf('p-dim-head') >= 0;
            })[indexAmongstValueCells];
        }

        if (!headerCell){
            return columnCells;
        }

        // find appropriate header cells. the lowermost row has the same columns as the values row; therefore- the cell index would be the same.
        var allDimensionHeaderCells = $(headersContainer).find('td.p-dim-member-head');

        if (headerCell.className.indexOf('p-dim-member-head') < 0) {
            // not a dimension member (a value header)- retrieve the correct dimension header using the measure path, if available
            headerCell = allDimensionHeaderCells.filter('[measurePath = "' + headerCell.getAttribute('measurePath') + '"]')[0] || headerCell;
        }

        var allCellsWithSameValue = getCellsFromSameFieldWithSameTextValue(allDimensionHeaderCells, headerCell);
        $.merge(columnCells, allCellsWithSameValue);

        var measurePath = headerCell.getAttribute('measurePath');
        if (!measurePath)
            return columnCells;

        var lastIndexOfComma = measurePath.lastIndexOf('","');
        while (lastIndexOfComma > 0) {
            measurePath = measurePath.slice(0, lastIndexOfComma + 1) + '}';
            headerCell = headersContainer.find('thead td[measurePath = "' + measurePath + '"]')[0];

            var allCellsWithSameValue = getCellsFromSameFieldWithSameTextValue(allDimensionHeaderCells, headerCell);
            $.merge(columnCells, allCellsWithSameValue);

            lastIndexOfComma = measurePath.lastIndexOf('","');
        }
        return columnCells;
    }

    function getMeasuredValueFilters(items, index){
        var context = $$get(_.find(items, function(item){return item.field.index == index}), "jaql.context");
        for (var prop in context) {
            if (context[prop].filter) {
                var newFilter = {};
                newFilter.filter = context[prop].filter;
                newFilter.dim = context[prop].dim;
                newFilter.datatype = context[prop].datatype;
                newFilter.title = context[prop].title;
                if (context[prop].level) {
                    newFilter.level = context[prop].level;
                }
                return {jaql: newFilter};

            }
        }
    }

    var lowermostCells = getLowermostHeaderCellsInOrder($(cell).parents('table'));
    var colCells = getColumnCells(cell, $(cell).parents('table'), lowermostCells);
    var rowCells = getRowCells(cell, $(cell).parents('table'));

    if(cell.className.indexOf('p-first-data-col') > -1){
        var itemsIndex = 1 + _.filter(widget.metadata.panel("columns").items,function(item){return item.disabled != true}).length;
    }
    else{
        var itemsIndex = parseFloat(cell.getAttribute('fidx'));
    }

    var filters = [];
    var measuredValueFilter = getMeasuredValueFilters(widget.metadata.panel('values').items, itemsIndex);

    if(measuredValueFilter){
        filters.push(measuredValueFilter)
    }

    $.merge(rowCells, colCells);
    var isGrandTotalRow;

    angular.forEach(rowCells, function (memberCell) {
        var fieldIndex;
        var items;
        isGrandTotalRow = false;

        if (memberCell.className.indexOf('p-measure-head') >= 0){
            fieldIndex = parseInt(memberCell.getAttribute('fDimensionIdx'));
            items = widget.metadata.panel('columns').items;
        }
        else if (memberCell.className.indexOf('p-dim-member-head') == 0){
            fieldIndex = parseInt(memberCell.getAttribute('fIdx'));
            items = widget.metadata.panel('columns').items;
        }
        else if(memberCell.className.indexOf('p-grand-total-row-head') > -1){
            fieldIndex = 0;
            isGrandTotalRow = true;
            items = widget.metadata.panel('rows').items;
        }
        else {
            fieldIndex = parseInt(memberCell.getAttribute('fIdx'));
            items = widget.metadata.panel('rows').items;
        }

        var filter = createFilter(items, fieldIndex, $(memberCell).attr('val'));

        if(isGrandTotalRow){  // for grand total row we don't want to always add 'include all' filter for row dimension
            var widgetFilters = widget.metadata.filters();
            var filtersWithoutDuplicates = getFiltersWithoutDuplicates(widgetFilters.concat(filter));
            if(filtersWithoutDuplicates.length <= widgetFilters.length){ // widget filter with such dimension already exists

                // don't move this filter to new dashboard because current widget
                // has a widget filter with more precise criterion which needs to be preserved.
                return;
            }
        }

        filters.push(filter);
    });

    return filters;
}
