// Drill To Dashboard Navigate Type Link Class



function DrillToDashboardNavigateTypeLink(widget){
    this.allowedTypes = ["pivot"];
    DrillToDashboardNavigateType.call(this, widget);

    //set widget events
    if (widget.$$events['ready'].handlers.indexOf(this.create) < 0 ) {
        widget.on("ready", this.create);
        widget.on("destroyed", removeOnReady);
    }

    // unregistering widget events
    function removeOnReady(widget, args) {
        widget.off("ready",  this.create);
        widget.off("destroyed", removeOnReady);
    }
}

DrillToDashboardNavigateTypeLink.prototype = Object.create(DrillToDashboardNavigateType.prototype);
DrillToDashboardNavigateTypeLink.prototype.constructor = DrillToDashboardNavigateTypeLink;

//create dashboard links with filters for each pivot cell
DrillToDashboardNavigateTypeLink.prototype.create = function(widget, args) {
    if (widget.options.drillTarget) {
        setTimeout(function () {
            //get all pivot cells
            var cells = $("td.p-value", $('#' + widget.oid));

            // if widget filter contains "include all" filter and dashboard filter contains filter for the same dimension,
            // then dashboard filter should take precedence (this may happen for grand total row cell click)
            var cleanIncludeAllWidgetFilters = function (widgetFilters, dashboardFilters) {
                dashboardFilters.forEach(function (dashboardFilterItem) {
                    if (dashboardFilterItem.isCascading) {
                        dashboardFilterItem.levels.forEach(function (level) {
                            tryRemoveIncludeAllFilter(level.dim, widgetFilters);
                        })
                    } else {
                        tryRemoveIncludeAllFilter(dashboardFilterItem.jaql.dim, widgetFilters);
                    }
                });
            };

            // remove widget includeAll filter of the same dimension if found
            var tryRemoveIncludeAllFilter = function (dashboardFilterItemDim, widgetFilters) {
                // finding same widget filter but which is set to includeAll
                var foundFilter = widgetFilters.filter(function (widgetFilterItem) {
                    return (widgetFilterItem.jaql.dim === dashboardFilterItemDim) && widgetFilterItem.jaql.filter.all;
                })[0];

                //remove existing widget filter
                if (foundFilter) {
                    widgetFilters.splice(widgetFilters.indexOf(foundFilter), 1);
                }
            };

            cells.each(function () {
                var pivotFilters = getCellFilters(widget, this);
                var dashboardFilters = getFilters(widget);
                cleanIncludeAllWidgetFilters(pivotFilters, dashboardFilters);
                var filters = mergeFilters(pivotFilters, dashboardFilters);

                $(this).data('filters', filters);

                function updateFilters() {
                    widget.drilledDashboardDisplay.display($(this).data('filters'), widget.drillToDashboardConfig, widget.options.drillTarget);
                }

                var clickHandlers = $$get($._data(this, "events"), 'click');

                if (!_.find(clickHandlers, function (handler) {
                        return handler.handler == updateFilters;
                    })) {
                    $(this).on("click", updateFilters);
                }

                if ($(this).attr("val") != "N\\A") {
                    if ($(this).find('span').length) {
                        $(this).find('span').html('<a href="javascript:void(0);">' + $(this).find('span').html() + '</a>');
                    }
                    else {
                        $(this).find('div.p-value').html('<a href="javascript:void(0);">' + $(this).find('div.p-value').text() + '</a>');
                    }
                }
                else {
                    $(this).find('div').html('<a href="javascript:void(0);">' + "0" + '</a>');
                }
            });
        }, 50);
    }
};