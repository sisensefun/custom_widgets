function DrillToDashboardNavigateTypeRightClick(widget) {
    if (widget.options.drillTarget) {
        this.allowedTypes = ["chart/pie", "chart/line", "chart/area", "chart/bar", "chart/column", "indicator", "pivot", "chart/scatter", "imageWidget", "richtexteditor"];

        this.initialize = function () {
            DrillToDashboardNavigateType.call(this, widget);

            var menuFactory = new RightClickMenuFactory();
            var rightClickMenuCreationFunction = menuFactory.getMenuCreationFunction(widget);

            if (rightClickMenuCreationFunction.name === "createCustomMenuItem") {
                rightClickMenuCreationFunction(widget);
                return;
            }

            var beforeMenuEventHandlers = $$get(prism.$ngscope.$$listeners["beforemenu"]);

            //check if event already registered
            if (beforeMenuEventHandlers && beforeMenuEventHandlers.filter(function (e) {
                    return (e && e.name == rightClickMenuCreationFunction.name)
                }).length > 0) {
                return;
            }

            //unregister events
            function removeOnBeforeMenu(widget) {
                beforeMenuHandler();
                widget.off("destroyed", removeOnBeforeMenu);
            }

            //set events
            var beforeMenuHandler = prism.on("beforemenu", function (ev, args) {

                if (!args.settings.widget)
                    return;

                var func = menuFactory.getMenuCreationFunction(args.settings.widget);
                if (func.name !== "createCustomMenuItem") {
                    func(ev, args);
                }
            });

            widget.on("destroyed", removeOnBeforeMenu);
        };
    }
}

DrillToDashboardNavigateTypeRightClick.prototype = Object.create(DrillToDashboardNavigateType.prototype);
DrillToDashboardNavigateTypeRightClick.prototype.constructor = DrillToDashboardNavigateTypeRightClick;