var allowedTypes = ["pivot", "chart/pie", "chart/line", "chart/area", "chart/bar", "chart/column", "indicator", "chart/scatter", "imageWidget", "richtexteditor"];
var otherTypes = ["indicator", "imageWidget", "richtexteditor"];

if(typeof main !== 'undefined') {

    main.directive("menuContent", [function () {
        return {
            restrict: 'C',
            link: function ($scope, lmnt, attrs) {
                if ($('.drillable').length > 0 && !$(lmnt).hasClass('drillList')) {
                    $(lmnt).addClass('drillList');                    
                    lmnt[0].style.opacity = 0; // hide to prevent flickering

                    window.setTimeout(function () {
                        //DOM has finished rendering
                        // now we can detect the frill menu dimensions and move it if necessary
                        var isLeft = $scope.$parent.$parent.isLeftSlideMenu;
                        if(isLeft){ // menu will appear to the left
                            //below is a huge hack to make the menu aligned correctly in all supported browsers
                            var container = $(lmnt).closest(".menu-item-host")[0];
                            var dataMenu = $(container).closest("data-menu")[0];
                            container.style.position = "absolute";
                            container.style.left = "-450px";
                            var width = container.offsetWidth;
                            container.style.left = "";
                            container.style.right = "0px"; // align to the right border. this will expand the container to required size.
                            container.style.top = $(dataMenu).find(".drillable").position().top  + "px";
                            var drillMenu = $(container).find(".drillList")[0];
                            var drillMenuStyle = getComputedStyle(drillMenu);
                            var borderWidth = (parseInt(drillMenuStyle["border-left-width"]) || 0) +  (parseInt(drillMenuStyle["border-right-width"]) || 0);
                            container.style.width = width + borderWidth + "px";
                            container.style.right = dataMenu.offsetWidth + "px";
                        }

                        lmnt[0].style.opacity = 1; // show element back
                    }, 0);
                }
            }
        }
    }]);
}
//hide drilled folders and dashboards for none owners
if (appConfig.hideDrilledDashboards) {
    navver.directive('listItemHolder', [
        function () {
            return {
                restrict: 'C',
                link: function link($scope, lmnt, attrs) {
                    if ($scope.listItem.title.match(appConfig.drilledDashboardPrefix)) {
                        if (prism.user._id != $scope.listItem.owner) {
                            $(lmnt).hide();
                        }
                    }
                }
            }
        }
    ]);
}

//variable for custom config
prism.JTDConfigs = [];

//load widget configuration
prism.jumpToDashboard = function (widget, args) {

    if (args === undefined){
        return;
    }

    var config = angular.extend(_.clone(appConfig), args);

    //save custom configuration from Edit Script
    prism.JTDConfigs[widget.oid] = config;

    widget.drillToDashboardConfig = config;
    widget.drillToDashboardConfig.custom = true;
};

prism.run([
    'widget-editor.services.$popper',
    function ($popper) {

        var originalPopMenuCustom;
        var isPhantom = window.navigator.userAgent.indexOf('phantom') >= 0;

        //adds support for Function.name ( IE issue )
        if (Function.prototype.name === undefined && Object.defineProperty !== undefined) {
            Object.defineProperty(Function.prototype, 'name', {
                get: function() {
                    var funcNameRegex = /function\s([^(]{1,})\(/;
                    var results = (funcNameRegex).exec((this).toString());
                    return (results && results.length > 1) ? results[1].trim() : "";
                },
                set: function(value) {}
            });
        }

        //removes temporary item from popup
        var removeTempItem = function(items){
            var tempItem = _.findWhere(items, {caption: 'Temporary_item'});
            if (tempItem) {
                items.splice(items.indexOf(tempItem), 1);
            }

        };

        // Registering widget events
        prism.on("dashboardloaded", function (e, args) {

            // Check if the filters should be reset to the state before the JTD
            if (!isPhantom && appConfig.resetDashFiltersAfterJTD && shouldUpdateFilters(args.dashboard)) {
                updateFilters(args.dashboard);
            }

            args.dashboard.on("widgetinitialized", function (dash,args) {
                if (!hasEventHandler(args.widget, "render", onWidgetRender)) {
                    args.widget.on('render', onWidgetRender);
                }
            });

            //skip, if already overridden
            if (!originalPopMenuCustom) {
                originalPopMenuCustom = $popper.popMenuCustom;

                //override popMenuCustom function
                $popper.popMenuCustom = function (items, ev, ok, ui, settings) {

                    //adds temp fake item to menu drill
                    if (settings && settings.hasOwnProperty('directive') && settings.directive === "<data-menu-drill></data-menu-drill>") {

                        items.push({
                            caption: "Temporary_item",
                            mid: "none"
                        });
                    }
                    originalPopMenuCustom(items, ev, ok, ui, settings);
                    removeTempItem(items);
                }
            }
        });

        function shouldUpdateFilters(dashboard){
            if (!defined(dashboard.openFromJTD)){
                return false;
            }

            if (dashboard.openFromJTD) {
                dashboard.openFromJTD = false;
                dashboard.$dashboard.updateDashboard(dashboard,"openFromJTD")
                return dashboard.openFromJTD;
            }

            return true;
        }

        function updateFilters(dashboard){
            var filtersToRestore = $$get(dashboard,"filtersToRestore");

            // Restore the original filters
            if (filtersToRestore){
                deleteDashboardAttributes(dashboard,["filtersToRestore","openFromJTD"]);

                var options = {
                    refresh: false,
                    save: true, //set false due to ISSUE in dashboard-updated event
                    unionIfSameDimensionAndSameType: true,
                    shouldResetNonSelectedDimensionsFilters: true,
                    reason: 'filtersUpdated'
                };

                dashboard.filters.clear();

                // update dashboard filters according to selected drill item
                dashboard.filters.update(filtersToRestore, options);
            }
        }

        function deleteDashboardAttributes(dashboard, attributes){
            attributes.forEach(function(att){
                dashboard[att] = null;
            });

            dashboard.$dashboard.updateDashboard(dashboard,attributes);
        }


        //add drilling image indicator
        function setDrillingImage(widget, args) {
            var widgetElement = getWidgetElement(widget),
                drillImageElement = widgetElement.find('#drillImg');

            if (!drillImageElement.length) {
                widgetElement.find('widget-dragger').after("<div id='drillImg' title='This widget is jumpable' />");
            }

            //target dashboard is set through widget menu or target dashboard ids is set through widget script
            if (args.widget.options.drillTarget || args.widget.drillToDashboardConfig.dashboardIds.length) {
                widgetElement.addClass('jumpable');

            } else {
                widgetElement.removeClass('jumpable');
            }
        }

        function onWidgetRender(widget, args) {
            //check if widget type is supported
            if (allowedTypes.indexOf(args.widget.type) == -1) {
                return;
            }

            var config = _.clone(appConfig);

            //adds custom configuration to widget
            if (prism.JTDConfigs[args.widget.oid]) {
                config = angular.extend(config, prism.JTDConfigs[args.widget.oid]);
            }

            args.widget.drillToDashboardConfig = config;

            if (!config.dashboardId) {
                new DrilledDashboardSelectionMenu(args.widget);
            }
            else {
                delete args.widget.options.drillTarget;
                new DrilledDashboardSelectionId(args.widget);
            }

            setDrillingImage(widget, args);

            if (!args.widget.options.drillTarget && !config.dashboardIds.length) {
                return;
            }

            // set custom navigation type for pivot widget
            if (args.widget.type == 'pivot' && config.drillToDashboardNavigateTypePivot) {
                args.widget.drillToDashboardConfig.drillToDashboardNavigateType = config.drillToDashboardNavigateTypePivot;
            }
            // set custom navigation type for chart widgets
            if ( args.widget.type.match("^chart") && config.drillToDashboardNavigateTypeCharts) {
                args.widget.drillToDashboardConfig.drillToDashboardNavigateType = config.drillToDashboardNavigateTypeCharts;
            }

            // set custom navigation type for other Type widgets
            if ( otherTypes.indexOf(args.widget.type) != -1 && config.drillToDashboardNavigateTypeOthers) {
                args.widget.drillToDashboardConfig.drillToDashboardNavigateType = config.drillToDashboardNavigateTypeOthers;
            }

            switch (config.drillToDashboardNavigateType) {
                case 2:
                    new DrillToDashboardNavigateTypeLink(args.widget);
                    break;
                case 3:
                    new DrillToDashboardNavigateTypeClick(args.widget);
                    break;
                default:
                    new DrillToDashboardNavigateTypeRightClick(args.widget).initialize();

            }
        }
    }
]);

// utility function to check if model contains certain event handler for specified event name
var hasEventHandler = function (model, eventName, handler) {
    return model.$$eventHandlers(eventName).indexOf(handler) >= 0;
};

var getDashboardFiltersToRestore = function(dashboardOID){
    // todo: check for v5.8
    //** SUPPORT FOR API 0.9 & 1.0 **//
    var api = '',
        apiType = 'GET',
        params = "",
        filters;

    //if API version > 5
    if(prism.version.split('.')[0]>5){
        api = 'v1/';
        // send the datasource due to a bug
        params = "?fields=filters%2Cdatasource%2CfiltersToRestore"
    }

    //open drilled dashboard modal window
    $.ajax({
        "url": "/api/" + api + "dashboards/" + dashboardOID + params,
        "type": apiType,
        async: false
    })
        .done(function (data) {
            filters = data.filtersToRestore || data.filters;
    });

    return filters;
}

function getWidgetElement(widget){
    if (prism.$ngscope.appstate == "widget"){
        return $(".widget-body");
    } else {
        return  $('widget[widgetid="' + widget.oid + '"]');
    }
}





