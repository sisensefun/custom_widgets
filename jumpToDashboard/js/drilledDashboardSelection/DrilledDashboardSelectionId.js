//Drilled Dashboard Selection Id Class



function DrilledDashboardSelectionId(widget){
    DrilledDashboardSelection.call(this, widget);

    //set drilled dashboard id
    widget.options.drillTarget = {oid: widget.drillToDashboardConfig.dashboardId, caption:  widget.drillToDashboardConfig.dashboardCaption};
}

DrilledDashboardSelectionId.prototype = Object.create(DrilledDashboardSelection.prototype);
DrilledDashboardSelectionId.prototype.constructor = DrilledDashboardSelectionId;