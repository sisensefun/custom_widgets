//Drilled Dashboard Selection Menu Class
function DrilledDashboardSelectionMenu(widget){

    var isExplorer = detectIE();

    var dashMenuSizeByDashTitle = [
        {
            className:"XLargeDrillSelected",size:"xl",margin:0,menuClass:"drillable",drillLeft:false,
            marginRichTextBox:0,sizeForReachTextBox:235,sizeForReachTextBoxNoFilterPanel: (isExplorer ? 0 : 200)
        },
        {
            className:"XXLargeDrillSelected",size:"xxl",margin:(isExplorer ? 0 : 195),menuClass:"drillable",drillLeft:true,
            marginRichTextBox:-690, sizeForReachTextBox:450,sizeForReachTextBoxNoFilterPanel:(isExplorer ? 0 : 200)
        }
    ];

    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */
    function detectIE() {
        var ua = window.navigator.userAgent;

        // Test values; Uncomment to check result �

        // IE 10
        // ua = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Trident/6.0)';

        // IE 11
        // ua = 'Mozilla/5.0 (Windows NT 6.3; Trident/7.0; rv:11.0) like Gecko';

        // IE 12 / Spartan
        // ua = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36 Edge/12.0';

        // Edge (IE 12+)
        // ua = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2486.0 Safari/537.36 Edge/13.10586';

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
        }

        var trident = ua.indexOf('Trident/');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
        }

        var edge = ua.indexOf('Edge/');
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
        }

        // other browser
        return false;
    }


    function calculateMEnuSizeToDashTitle(titleLength){
        if (titleLength <= 26){
            return dashMenuSizeByDashTitle[0];
        } else{
            return dashMenuSizeByDashTitle[1];
        }
    }

    //check if event already registered
    if (defined(prism.$ngscope.$$listeners.beforemenu) && prism.$ngscope.$$listeners.beforemenu.filter(function (e){return (e &&  e.name == 'setDashboardsMenu')}).length > 0) {
        return;
    }

    DrilledDashboardSelection.call(this, widget);

    //remove events
    function removeOnBeforeMenu(widget, args) {
        var eventdef = $$get(prism.$ngscope.$$listeners["beforemenu"]);
        var index = eventdef.indexOf(setDashboardsMenu);

        eventdef.splice(index, 1);
        widget.off("destroyed", removeOnBeforeMenu);
    }

    //set events
    prism.on("beforemenu", setDashboardsMenu);
    widget.on("destroyed", removeOnBeforeMenu);

    //set dashboards menu
    function setDashboardsMenu (e, args) {
        var target = $(args.ui.target);

        // Check if user in view mode. First Check if for v6.2, second check is for v5.8
        var isUserNotAllowToChangeJTD = $$get(prism.activeDashboard.userAuth,'base.isConsumer') ||
            !(prism.activeDashboard.userAuth.widgets.edit && prism.activeDashboard.userAuth.widgets.edit_script);

        if (isUserNotAllowToChangeJTD){
            return;
        }

        if ((!target.hasClass('wet-menu') && !target.find('.menu-btn').length ) || target.is('widget')) {
            return;
        }

        //saves widget configurations
        var
            appstate = e.currentScope.appstate,
            widget = (appstate === 'widget') ? e.currentScope.widget : args.settings.scope.widget;

        if( !widget ||
            /*adds menu on dashboard to widget only for richtexteditor*/
            (widget.type !== 'richtexteditor' && appstate === 'dashboard') ||
            /*if a drilled dashboard defined as a widget configuration, don't set the menu:*/
            (defined(widget.drillToDashboardConfig.dashboardId))){
            return;
        }

        //get drilled dashboards
        var drilledDashboards = getDashboards(widget.dashboard.oid, widget.options.drillTarget);

        //get drill menu item
        var drillMenuItem = _.find(args.settings.items,function(item){return item.caption === widget.drillToDashboardConfig.drillToDashboardMenuCaption});
        var widgetElement = getWidgetElement(widget);
        var isLeft;
        var isTextWidget = widget.type === 'richtexteditor';

        if (drilledDashboards.items.length) {
            if (widgetElement.length > 0)
            {
                isLeft = widgetElement.offset().left + widgetElement.width() - 235 > drilledDashboards.design.sizeForReachTextBox;
            }

            //update dashboards list
            if (drillMenuItem) {
                drillMenuItem.items = drilledDashboards;
            }
            //add drill menu item
            else {
                args.settings.isLeftSlideMenu =  drilledDashboards.design.drillLeft;
                args.settings.items.push({type: "separator"});
                args.settings.items.push({
                    caption: widget.drillToDashboardConfig.drillToDashboardMenuCaption,
                    drillLeft: isTextWidget ? (drilledDashboards.design.drillLeft && isLeft) : drilledDashboards.design.drillLeft,
                    items: drilledDashboards.items.concat([{type: "isLeftSlide", disabled: true}]),
                    classes: drilledDashboards.design.menuClass,
                    margin:  drilledDashboards.design.drillLeft ? -235 : 0
                });
            }
        }

        //save 'drill to dashboard' id
        function drillTo() {
            if (this.checked) {
                delete widget.options.drillTarget;
                this.checked = false;

                var element = getWidgetElement(widget);

                try {
                    $(element).unbind("click", $._data($(element)[0]).handlerFunc);
                    $(element).removeClass("clickable");
                } catch(ex){}
            }
            else {
                widget.options.drillTarget = {oid: this.oid, caption: this.caption, folder: this.folder};
            }

            if(widget.type === 'richtexteditor') {
                // saves all changes to mongo
                widget.changesMade("drillTarget", 'options');
            }

            widget.refresh();
        }

        //get drilled dashboards with the same data source
        function getDashboards(currDS, drillTarget) {

            var drillMenuItems = [],
                folderIds = [],
                largestDesignIndex = 0,
                design = {};

            // get all folders ids matches drilledDashboardPrefix
            $.ajax({
                type: "GET",
                url: "/api/folders",
                async: false
            }).done(function (data) {
                data.forEach(function (item) {
                    folderIds.push({oid:item.oid,name:item.name});
                });
            });

            //** SUPPORT FOR API 0.9 & 1.0 **//
            var api = '',
                params = '';

            //if API version > 5
            if(prism.version.split('.')[0]>5){
                api = 'v1/';
                params = '?fields=oid,title,parentFolder,datasource';
            }

            //get all dashboards with drilledDashboardPrefix or dashboards inside a folder matches drilledDashboardPrefix
            $.ajax({
                type: "GET",
                url: "/api/" + api + "dashboards" + params,
                async: false
            }).done(function (data) {
                _.each(data,function(item){
                    var title;
                    var folder = _.find(folderIds, function (folder) {
                        return folder.oid === item.parentFolder;
                    });

                    if (item.oid != currDS &&
                        item.datasource.id == widget.dashboard.datasource.id &&
                        item.title.match(widget.drillToDashboardConfig.drilledDashboardPrefix)){
                        if (item.parentFolder && appConfig.showFolderNameOnMenuSelection) {
                            title = folder.name + "\\" + item.title;
                        } else {
                            title = item.title;
                        }

                        var menuItemDesign = calculateMEnuSizeToDashTitle(title.length);
                        largestDesignIndex = dashMenuSizeByDashTitle.indexOf(menuItemDesign) > largestDesignIndex ? dashMenuSizeByDashTitle.indexOf(menuItemDesign) : largestDesignIndex;

                        drillMenuItems.push({
                            caption: title,
                            type: "check",
                            execute: drillTo,
                            checked: (defined(drillTarget) && item.oid == drillTarget.oid) ? true : false,
                            oid: item.oid,
                            folder: item.parentFolder
                        });
                    }
                });

                $.extend(design,dashMenuSizeByDashTitle[largestDesignIndex],true);
            });

            return {items:drillMenuItems,design:design};
        }
    }
}

DrilledDashboardSelectionMenu.prototype = Object.create(DrilledDashboardSelection.prototype);
DrilledDashboardSelectionMenu.prototype.constructor = DrilledDashboardSelectionMenu;