//Drilled Dashboard Display Class

function DrilledDashboardDisplay() {}

DrilledDashboardDisplay.prototype.url = "";
DrilledDashboardDisplay.prototype.payload = null;

//display the drilled dashboard
DrilledDashboardDisplay.prototype.display = function(filters, drillToDashboardConfig, drilledDashboard){

    // Set parameters to url
    var dashboardOptionsStr = setParamsToUrl(drillToDashboardConfig);

    var iframeSeconds = '';

    //add seconds to URL before hash, for nested iframes. only for popup. Parameter does not affect on Angular routing
    if(drillToDashboardConfig.drilledDashboardDisplayType == 2) {
        iframeSeconds += '?t=' + Math.floor(Date.now() / 1000);
    }

    //set dashboard URL
    this.url = "/app/main"+iframeSeconds+"#/dashboards/" + drilledDashboard.oid + "?" + dashboardOptionsStr;

    if(typeof drillToDashboardConfig.excludeFilterDims === 'string'){
        drillToDashboardConfig.excludeFilterDims = drillToDashboardConfig.excludeFilterDims.split(',');
    }
    //remove excluded dimensions from filters
    var excludeFilterDims = drillToDashboardConfig.excludeFilterDims.map(function(dim){return dim.toLowerCase()});

    filters = filters.filter(function(filterItem){
        return filterItem.isCascading /*skip cascading filters*/ || (!filterItem.jaql.agg && filterItem.jaql.dim && excludeFilterDims.indexOf(filterItem.jaql.dim.toLowerCase()) < 0);
    });

    var excludedFiltersConditionFunction = function(level){
        return excludeFilterDims.indexOf(level.dim.toLowerCase()) >= 0
    };

    this.updateCascadingFilters(filters, excludedFiltersConditionFunction);

    if(typeof drillToDashboardConfig.includeFilterDims === 'string'){
        drillToDashboardConfig.includeFilterDims = drillToDashboardConfig.includeFilterDims.split(',');
    }

    //select only selected dimension in filters
    var includeFilterDims = drillToDashboardConfig.includeFilterDims.map(function(dim){return dim.toLowerCase()});

    if (includeFilterDims.length > 0) {
        var includedFiltersConditionFunction = function(level){
            return includeFilterDims.indexOf(level.dim.toLowerCase()) < 0;
        };

        this.updateCascadingFilters(filters, includedFiltersConditionFunction);


        // include dimensions from other filters
        filters = filters.filter(function(filterItem){
            return filterItem.isCascading /*skip cascading filters*/ || includeFilterDims.indexOf(filterItem.jaql.dim.toLowerCase())>= 0;
        });

    }

    var payload = {filters:filters};

    // Adds the filters to restore to the dashboard
    if ($$get(drillToDashboardConfig,"resetDashFiltersAfterJTD")){
        payload.filtersToRestore = getDashboardFiltersToRestore(drilledDashboard.oid);
        payload.openFromJTD = true;
    }

    //set payload
    this.payload = angular.toJson(payload).replace("&amp;","&").replace(/'/g, '%27');
};

function setParamsToUrl(drillToDashboardConfig){
    var paramsArr = [];

    if (!drillToDashboardConfig.displayFilterPane){
        paramsArr.push("r=false");
    }

    if (!drillToDashboardConfig.displayDashboardsPane){
        paramsArr.push("l=false");
    }

    if (!drillToDashboardConfig.displayToolbarRow){
        paramsArr.push("t=false");
    }

    if (!drillToDashboardConfig.displayHeaderRow){
        paramsArr.push("h=false");
    }

    if (drillToDashboardConfig.volatile) {
        paramsArr.push("volatile=true");
    }

    return paramsArr.join("&");
}

DrilledDashboardDisplay.prototype.updateCascadingFilters = function(filters, conditionToRemoveLevel){
    //remove excluded dimensions from cascading filters
    for (var id = filters.length - 1; id >= 0; id--) {
        var filterItem = filters[id];

        if(filterItem.isCascading){

            var levels = filterItem.levels;

            for (var i = levels.length - 1; i >= 0; i--) {

                if(conditionToRemoveLevel(levels[i])) {
                    levels.splice(i, 1); // remove level if dimension matches
                }
            }
            //changes cascading filter to non cascading
            if(levels.length == 1){
                filterItem.jaql = levels[0];
                delete filterItem.levels;
                delete filterItem.model;
                filterItem.isCascading = false;
            }

            if(!levels.length){ // remove filter if no levels remain
                filters.splice(id, 1);
            }
        }
    }
};