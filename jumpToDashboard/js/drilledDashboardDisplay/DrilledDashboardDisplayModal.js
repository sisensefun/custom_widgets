// Drilled Dashboard Display Modal class

function DrilledDashboardDisplayModal(){
    DrilledDashboardDisplay.call(this);
}

DrilledDashboardDisplayModal.prototype = Object.create(DrilledDashboardDisplay.prototype);
DrilledDashboardDisplayModal.prototype.constructor = DrilledDashboardDisplayModal;



//display the drilled dashboard in modal window
DrilledDashboardDisplayModal.prototype.display = function(filters, drillToDashboardConfig, drilledDashboard){
    DrilledDashboardDisplay.prototype.display.call(this, filters, drillToDashboardConfig, drilledDashboard);

    //set modal window width
    if (!defined(drillToDashboardConfig.modalWindowWidth)) {
        var width = Math.max(Math.floor($(window).outerWidth() * 5 / 6), 500);
    }
    else{
        var width = drillToDashboardConfig.modalWindowWidth
    }

    //set modal window height
    if (!defined(drillToDashboardConfig.modalWindowHeight)) {
        var height = Math.max(Math.floor($(window).outerHeight() * 5 / 6), 400);
    }
    else{
        var height = drillToDashboardConfig.modalWindowHeight;
    }

    // create the modal template
    var iframeDiv ='<iframe src="'+ this.url +'" width="' + width + 'px" height="' + height + 'px" ></iframe>';

    // get the dom service
    var $dom = prism.$injector.get("ux-controls.services.$dom");

    // create the settings
    var settings = {template : iframeDiv, scope:'fake'}; // Hack for $dom service

    //** SUPPORT FOR API 0.9 & 1.0 **//
    var api = '';
    var apiType = 'PUT';

    //if API version > 5
    if(prism.version.split('.')[0]>5){
       api = 'v1/';
       apiType = 'PATCH';
    }

    //open drilled dashboard modal window
    $.ajax({
        "url"       : "/api/" + api + "dashboards/" + drilledDashboard.oid,
        "type"      : apiType,
        "data"      : this.payload,
        contentType : "application/json;charset=UTF-8",
        success:function(){
            // create modal
            $dom.modal(settings);

            // HACK: sets event listener for overlay div, and fires the $dom click event
            var overlay = $('div.md-overlay');
            overlay.bind('click', function(){
                $('body').trigger('click');
                overlay.unbind('click',arguments.callee);
            });
        }
    });
};
