// Drilled Dashboard Display Tab class

function DrilledDashboardDisplayTab(currentTab){
    DrilledDashboardDisplay.call(this);

    //set tab position
    if (currentTab){
        this.self = "_self";
    }
    else{
        this.self = "";
    }
}

DrilledDashboardDisplayTab.prototype = Object.create(DrilledDashboardDisplay.prototype);
DrilledDashboardDisplayTab.prototype.constructor = DrilledDashboardDisplayTab;



DrilledDashboardDisplayTab.prototype.self = "";

//display the drilled dashboard in a tab
DrilledDashboardDisplayTab.prototype.display = function(filters, drillToDashboardConfig, drilledDashboard){
    DrilledDashboardDisplay.prototype.display.call(this, filters, drillToDashboardConfig, drilledDashboard);

    var url = this.url;
    var self = this.self;

    //** SUPPORT FOR API 0.9 & 1.0 **//
    var api = '';
    var apiType = 'PUT';

    //if API version > 5
    if(prism.version.split('.')[0]>5){
        api = 'v1/';
        apiType = 'PATCH';
    }

    //open drilled dashboard window
    $.ajax({
        "url"       : "/api/" + api + "dashboards/" + drilledDashboard.oid,
        "type"      : apiType,
        "data"      : this.payload,
        contentType : "application/json;charset=UTF-8",
        success:function(){

            // Get parameters of the current location
            var oldParams = getParams(location.href);

            // Get parameters of the new location
            var newParams = getParams(url);

            // We do not have parameters so we do not need to refresh the page
            if (oldParams === newParams){
                window.open(url, self);
            }
            // The parameters are not equal so we need to refresh the page
            else {
                location.href = url;
                location.reload();
            }
        }
    });

    function getParams(url){
        var index = url.indexOf("?");
        return index > -1 ? url.substring(index + 1,url.length) : "";
    }
};
