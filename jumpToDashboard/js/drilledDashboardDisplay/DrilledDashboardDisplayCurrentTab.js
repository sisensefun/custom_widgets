// Drilled Dashboard Display Current Tab class

function DrilledDashboardDisplayCurrentTab(){
    DrilledDashboardDisplayTab.call(this, true);
}

DrilledDashboardDisplayCurrentTab.prototype = Object.create(DrilledDashboardDisplayTab.prototype);
DrilledDashboardDisplayCurrentTab.prototype.constructor = DrilledDashboardDisplayCurrentTab;

