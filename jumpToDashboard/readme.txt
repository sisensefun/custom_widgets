#Drill to dashboard supports charts, pivots and indicators

For pivots supports: dashboard filters, widget filters, measured values.
For charts support: dashboard filters, widget filters.

#Steps to implement:
1. Extract the folder to prismweb/plugins
2. Create a dashboard with prefix _drill_ or folder with  prefix _drill_
3. Go to the source dashboard and in the widget you want to drill from go to its edit window and in the menu you will have drill to dashboard options which you can choose to which
dashboard to jump to, select a target dashboard
4. Click apply
5. Right click on a point/cell and choose "drill to dashboard"


#Changing default settings:

- Edit widget script: prism.jumpToDashboard(widget, args)

- Set args to the parameter you'd like to update:
    1.  Set drilledDashboardPrefix to empty string - will cause all dashboards to be potential drilled dashboards:
        prism.jumpToDashboard(widget, { drilledDashboardPrefix  : ""});
    2. Exclude brand dimension from the drilled dashboard filters:
        prism.jumpToDashboard(widget, { excludeFilterDims  : "[brand.Brand]"});

- Set multiple target dashboards (NOTE: not applicable to pivot link navigation type):
     Add widget script like the following:
     prism.jumpToDashboard(widget, {
                                        dashboardIds  : [
                                                            {id:"568a8d9eaca677c414000001", caption:"Dashboard Name"},
                                                            {id:"123a8d9f3fd678c433000003", caption:"Other New Name"}
                                                        ]
                                   }
                           );


#Settings:

Key                                 Description                                                                         Type                        DefaultValue
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
drilledDashboardPrefix              The prefix of the drilled dashboards name or folder.                                string                      "_drill_"
displayFilterPane                   Display filter pane in the drilled dashboard window.                                boolean                     true
displayDashboardsPane               Display dashboards pane in the drilled dashboard window.                            boolean                     true
displayToolbarRow                   Display toolbar in the drilled dashboard window.                                    boolean                     true
displayHeaderRow                    Display header in the drilled dashboard window.                                     boolean                     true
volatile                            Volatile for the drilled dashboard window.                                          boolean                     true
hideDrilledDashboards               Hide drilled dashboards from the dashboards navigator for non owner users.          boolean                     true
drillToDashboardMenuCaption         The caption for the drill to dashboard menu                                         string                      "Jump to dashboard"
drillToDashboardRightMenuCaption    The caption for the drill to dashboard right menu                                   string                      "Jump to dashboard"
drillToDashboardNavigateType        How to navigate to the configured drilled dashboard                                 NavigateTypeId (See Bellow) 1
drillToDashboardNavigateTypePivot   Custom navigation type for pivot widget                                             NavigateTypeId (See Bellow) 1
drillToDashboardNavigateTypeCharts  Custom navigation type for chart widgets                                            NavigateTypeId (See Bellow) 1
drillToDashboardNavigateTypeOthers  Custom navigation type for other widgets (indicator, richtexteditor, imageWidget)   NavigateTypeId (See Bellow) 1
excludeFilterDims                   Dimensions to exclude from the drilled dashboard filter                             Array of DIM (See Bellow)   []
includeFilterDims                   Dimensions to include in the drilled dashboard filter                               Array of DIM (See Bellow)   []
drilledDashboardDisplayType         How to display the drilled dashboard                                                DisplayTypeId (See Bellow)    1
dashboardId                         Drilled dashboard id.
                                    When set to null, a drilled dashboard menu will be avilable in the widget editor,   string                      null
                                    allowing the user to choose the drilled dashboard from the menu.
dashboardIds                        Multiple target Dashboards                                                          Array of objects            []
                                                                                                                        {id:"", caption:""}
dashboardCaption                    Drilled dashboard caption.                                                          string                      null
modalWindowWidth                    Modal window width, In case the selected display type is modal window.              number                      null
modalWindowHeight                   Modal window height, In case the selected display type is modal window.             number                      null


DIM:
string represent dimension: "[table.dimension]".
Example: "[brand.Brand]"

Navigate Type:

Id  Description                                     Supported Widget Types
------------------------------------------------------------------------------
1   Right click on a pivot cell/ point or indicator Pivot, Indicator, Charts ,Rich Text Editor, Image Widget
2   Link on pivot measured cells                    Pivot
3   Click on custom widget                          Indicator ,Rich Text Editor, Image Widget


Display Type:

Id  Description
-----------------
1   New tab
2   Popup window
3   Current tab


