//save the widget context from the icon menu
mod.directive('iconMenu', function(){    
	return{		
		restrict: 'C',
		scope:{

		},	
		link: function (scope, element, attrs) {	

			element.parent().on('click', function () {
				window.menuOpenedWidgetID = scope.$parent.widget.oid;
				window.menuOpenedWidgetType = scope.$parent.widget.type;
			});
		}
	}
});

//	Attach the 
mod.directive('menuContent', function(){    
	return{	
		restrict: 'C',
		scope:{
		},	
		link: function (scope, element, attrs) {
			// unsupported types : ["Indicator","Pivot","Table","Treemap","Scatter map","Area map"]
			var supportedWidgets =["chart/line","chart/pie","chart/line","chart/column","chart/bar","chart/area","chart/scatter","heatmap","sunburst","chart/polar"],
				isSupportedType = _.find(supportedWidgets,function(type){
					return window.menuOpenedWidgetType.toLowerCase() === type.toLowerCase();
				});

			//	Only run if the widgetID is defined AND if not a map
			if (!window.menuOpenedWidgetID || !isSupportedType){
				return;
			}
			
			// find the widget in the dashboard
			var widget = _.find(scope.$root.dashboard.widgets.$$widgets, function(w){
				return w.oid == window.menuOpenedWidgetID;
			});

			if(widget == undefined){
				return;
			}
			
			//	Define the export function
			var directPrint = function() {

				//	Find the widget
				var id = this.widgetID;
				var widgetHTML = $('widget[widgetid="' + id + '"]').clone(true);

				//	Strip out the widget header
				$('widget-header',widgetHTML).remove();

				// remove the footer
				if ($(widgetHTML).find("div.cancel-footer")){
					$(widgetHTML).find("div.cancel-footer").remove();
				}

				//	Create the widget title
				var title = widget.desc ? widget.title + ": " + widget.desc	: widget.title;

				if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
					var popupWin = window.open('', '_blank', 'width=600,height=600,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
					popupWin.window.focus();
					popupWin.document.write('<!DOCTYPE html><html><head>' +
							'<title>' + title + '</title>' +
						'<link rel="stylesheet" type="text/css" href="style.css" />' +
						'</head><body onload="window.print()"><div class="reward-body">' + widgetHTML.html() + '</div></html>');

					popupWin.onbeforeunload = function (event) {
						popupWin.close();
						return '.\n';
					};
					popupWin.onabort = function (event) {
						popupWin.document.close();
						popupWin.close();
					}
				} else {
					var popupWin = window.open('', '_blank', 'width=800,height=600');
					popupWin.document.open();
					popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + widgetHTML.html() + '</html>');
					popupWin.document.close();
				}

				popupWin.document.close();
			};
			
			//	Define export command
			var exportCommand = {
				$$hashKey : "079",
				caption : "Direct Print",
				command:{
					canExecute:function(){return true;},
					execute: directPrint,
					title: "Direct Print",
					disabled: false,
					tooltip : "Print Widget to Printer",
					widgetID : window.menuOpenedWidgetID
				}
			}
			
			// Define separator line
			var separator = {
				type: "separator"
			};
			
			//	Add export command to the actions list
			scope.$parent.items.unshift(separator);	
			scope.$parent.items.unshift(exportCommand);		
			
			//	Remove widget ID from window object
			window.menuOpenedWidgetID = null;
		}
	}
});

