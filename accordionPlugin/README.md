# Example of usage

1). Open "Edit script" window of widget.

2). Add following script:

```
#!javascript

widget.on('ready', function(){

	Accordion({
		//default: true, // uncomment this to make accordion to open on start
		element: element,
		widget: widget,
		dashboard: dashboard,
		filters: ['[<TABLE_NAME>.<FIELD_NAME>]'],
		dashboardUrl: 'http://localhost/app/main#/dashboards/<YOUR_DASHBOARD_ID>'
	});
})

```

You can specify dashboardName instead of dashboardUrl. In this case the configuration will be like the following:


```
#!javascript

widget.on('ready', function(){

	Accordion({
		//default: true, // uncomment this to make accordion to open on start
		element: element,
		widget: widget,
		dashboard: dashboard,
		filters: ['[<TABLE_NAME>.<FIELD_NAME>]'],
		dashboardName: '_accrd_myDashboardName'
	});
})

```


3). Save it, close the window. Apply changes for widget and reload the page.

4). Click on the widget and wait for dashboard loads.

5). Profit!
