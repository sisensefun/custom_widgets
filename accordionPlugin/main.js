/**
 * Accordion Plugin for Booking.com
 *
 * Dependencies:
 *   - jQuery
 *   - underscore.js
 *
 * @author Volodymyr Sheremeta <volodymyr.sh@sisense.com>
 */

//hide accordion folders for none owners
navver.directive('listItemHolder', [
    function () {
        return {
            restrict: 'C',
            link: function link($scope, lmnt, attrs) {
                if ($scope.listItem.title.match("^_accrd_")) {
                    if (prism.user._id != $scope.listItem.owner) {
                        $(lmnt).hide();
                    }
                }
            }
        }
    }
]);


(function (ns) {
    'use strict';

    ns.Accordion = function (params) {
        if(!params.element){
            console.warn("Accordion plugin: No 'Element' attribute is passed from widget script'");
        }

        var
            element = $(params.element),
            widget = params.widget,
            dashboard = params.dashboard,
            filters = lowerCaseFilters(params.filters),
            dashboardUrl = params.dashboardUrl,
            dashboardName = params.dashboardName,
            defaultHeight = 500, // This sets the initial height of the iFrame
            defaultPaddingTop = 30,
            paddingString = '26px 4px 4px 4px',
            accorId = 'bk-id-' + widget.oid,
            pageY = null,
            currentHeight = null,
            iframeWrapper = null,
            filterString = null,
            parent =  element.parents('.dashboard-layout-cell'),
            arrow = element.siblings('.bk-arrow'),

            clickHandler = function () {
                var accordions = $('.bk-accordion');

                if (iframeWrapper.data('opened') || !parent.hasClass('dashboard-layout-cell')) {
                    closeAccordions(accordions, iframeWrapper);
                } else {
                    accordions.data('opened', false);

                    var iFrameWrapperHeight = defaultHeight + defaultPaddingTop;

                    updateColumnContainerHeight(0, iFrameWrapperHeight);

                    var src = getIFrameSrc();

                    arrow.css('opacity', 1);
                    iframeWrapper
                        .data('opened', true)
                        .removeClass('no-transition')
                        .css({
                            padding: paddingString,
                            opacity: 1,
                            height: iFrameWrapperHeight + 'px'
                        });

                    iframeWrapper.find('iframe')
                        .css('height', defaultHeight + 'px');

                    if (!isSameIFrameSrc(iframeWrapper, src))
                        reloadIframe(iframeWrapper, src);


                }
            },

            reloadIframe = function (iframeWrapper, src) {
                var iframe = iframeWrapper.find('iframe')[0];
                iframe.setAttribute('src', src);
            },

            dashboardRefreshStartHandler = function () {
                var src = getIFrameSrc();
                reloadIframe(iframeWrapper, src);
                dashboard.off('refreshstart', dashboardRefreshStartHandler);
            },

            splitLocation = function (src) {
                var result = {"location": "", hash: ""};
                var splitted = src.split("#");
                result.location = splitted[0];
                if(splitted.length === 2){
                    result.hash = "#" + splitted[1];
                }
                return result;
            },

            getIFrameSrc = function () {
                var queryParams = '?h=false&t=false&l=false&r=false&volatile=true&embed=true';
                var src = dashboardUrl + queryParams;
                var selectedFilters = [];
                var tempFilters = [];
                var dim1Found,dim2Found;
                if (filters.length) {
                    dashboard.filters.$$items.forEach(function (filter) {
                        filter.getJAQL().forEach(function (jaql) {
                            for(var j=0;j < filters.length;j++){
                                if(typeof filters[j] == 'object'){
                                    if(jaql.dim.toLowerCase() == filters[j].dim1){
                                        dim1Found = true;
                                        tempFilters.push(filter);
                                    }
                                    if(jaql.dim.toLowerCase() == filters[j].dim2){
                                        dim2Found = true;
                                        tempFilters.push(filter);
                                    }
                                }
                                else if (filters[j].indexOf(jaql.dim.toLowerCase())){
                                    selectedFilters.push(filter);
                                }

                            }

                        });
                    });
                    if(dim1Found && dim2Found && tempFilters.length){
                        tempFilters[0].jaql.filter.members = tempFilters[0].jaql.filter.members.concat(tempFilters[1].jaql.filter.members);
                        selectedFilters.push(tempFilters[0])
                    }
                } else {
                    selectedFilters = dashboard.filters.$$items;
                }

                filterString = encodeURIComponent(JSON.stringify(selectedFilters));
                var result = src + '&filter=' + filterString;

                var is_firefox = /firefox/i.test(navigator.userAgent);
                var isIE = /MSIE|Trident|Edge/.test(navigator.userAgent);

                if(is_firefox || isIE){ // hack for firefox and IE
                    var location = splitLocation(result);
                    result = location.location + "/" + location.hash;
                }

                return result;
            },

            closeAccordions = function (accordions, currentAccordion) {
                var css = {
                    opacity: 0,
                    padding: 0,
                    height: 0
                };

                iframeWrapper.data('opened', false);
                var heightBeforeClosing = currentAccordion[0].offsetHeight;

                accordions && accordions.not('#' + accorId).css(css).addClass('no-transition');
                currentAccordion && currentAccordion.css(css);
                updateColumnContainerHeight(heightBeforeClosing, 0);

                $('.bk-arrow').css('opacity', 0);
            },

            // column container's height sholud be in sync with the height of the accordion's iFrame container
            updateColumnContainerHeight = function (from, to) {
                var columnContainer = parent.parent();
                columnContainer.css("height", columnContainer.height() + (to - from) + "px");
            },

            createIFrame = function (accorId) {
                iframeWrapper = $('#' + accorId);

                if (!arrow.length){
                    arrow = $('<div class="bk-arrow"></div>');
                    element.after(arrow);
                }

                if (!iframeWrapper.length) {
                    iframeWrapper = $('<div class="bk-accordion">')
                        .attr('id', accorId)
                        .append('<div class="bk-wrapper">')
                        .append('<div class="bk-resize">')
                        .append('<div class="bk-close">');

                    iframeWrapper.find('.bk-wrapper')
                        .append('<div class="bk-loader"></div>')
                        .append($('<iframe>', {
                            width: '100%',
                            height: defaultHeight + 'px'
                        }));

                    iframeWrapper.find('.bk-close').on('click', function () {
                        closeAccordions(null, iframeWrapper);
                    });

                    initResize();
                }

                if (!$.contains(document, iframeWrapper[0])) {
                    parent.after(iframeWrapper);
                }
            },

            isSameIFrameSrc = function(iframeWrapperElement, newSrc){
                //return iframeWrapperElement.find('iframe')[0].contentWindow.location.href === newSrc
                return iframeWrapperElement.find('iframe').attr("src") === newSrc;
            },

            initResize = function () {
                var dragging = false;
                iframeWrapper.find('.bk-resize').on("mousedown", function (e) {
                    dragging = true;
                    pageY = e.pageY;
                    currentHeight = parseInt(iframeWrapper.css('height'), 10);
                    iframeWrapper.addClass('no-transition-resize').find('iframe').hide(0);
                });

                $(document).on("mousemove", function (e) {
                    if (dragging) {
                        var newHeight = currentHeight - (pageY - e.pageY);
                        iframeWrapper.css('height', newHeight + 'px')
                            .find('iframe').css('height', (newHeight - defaultPaddingTop) + 'px');


                    }
                });

                $(document).on("mouseup", function (e) {
                    if (dragging) {
                        iframeWrapper.removeClass('no-transition-resize').find('iframe').show(0);
                        defaultHeight = parseInt(iframeWrapper.find('iframe').css('height'));
                        dragging = false;
                        updateColumnContainerHeight(currentHeight, iframeWrapper[0].offsetHeight);
                    }
                });
            },

            init = function () {
                if (!element.length) return;

                createIFrame(accorId);
                parent.after(iframeWrapper);

                element.off('click');
                element.on('click', clickHandler);
                element.addClass("clickable");

                // if there "default" param - open widget once on the dashboard init
                if (params.default && !prism['isAccordionOpened_' + widget.oid]) {
                    prism['isAccordionOpened_' + widget.oid] = true;
                    clickHandler();
                }

                widget.on('destroyed', function () {
                    delete prism['isAccordionOpened_' + widget.oid];
                });
                dashboard.on('refreshstart', dashboardRefreshStartHandler);
            };

        function lowerCaseFilters(filters) {
            return (filters || []).map(function (n) {
                if(typeof n === 'object'){
                    n.dim1 = n.dim1.toLowerCase();
                    n.dim2 = n.dim2.toLowerCase();
                    return n;
                }
                else{
                    return n.toLowerCase();
                }
            })
        }

        if(dashboardName && !dashboardUrl){
            //If dashboard name is supplied - determine dashboard Id dynamically
            $.ajax({
                method: "GET",
                url: "/api/v1/dashboards/?fields=title,oid"
            })	//	Function to run when complete
                .done(function(dashboards) {
                    var dashboard = dashboards.filter(function(item) { return item.title === dashboardName; });
                    var dashboardId = dashboard[0].oid; // take the first of dashboard with the same name
                    var getUrl = window.location;
                    var baseUrl = getUrl.protocol + "//" + getUrl.host + "/app/main/#/dashboards/";
                    dashboardUrl = baseUrl + dashboardId;
                    init();
                })
        }else{
            init();//initializing plugin
        }
    };

}(window));